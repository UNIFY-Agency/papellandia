<?php

class Checkout{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //status 1 = aguardando pagamento
    //status 2 = pago
    //status 3 = entrega
    //status 4 = finalizado
    //status 0 = cancelado

    //entrega 1 = delivery
    //entrega 2 = take away
    //entrega 3 = adiantamento delivery
    //entrega 4 = adiantamento take away

    public function getPaymentMethods(){
      $select = $this->mysql->prepare('SELECT * FROM payment_methods WHERE 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getPaymentMethodsById($id){
      $select = $this->mysql->prepare('SELECT * FROM payment_methods WHERE id = :id');
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getAllowCountries(){
      $select = $this->mysql->prepare('SELECT p.nome, p.codigo, pp.id_pais, pp.portes FROM portes_paises pp INNER JOIN paises p ON pp.id_pais = p.id');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function linkCupomToOrder($id_pedido, $id_cupom){
      $cadastra = $this->mysql->prepare('INSERT INTO `pedidos_cupom`(`id_pedido`, `id_cupom`, `criado`) VALUES (:id_pedido, :id_cupom, :criado);');
      $cadastra->bindValue(':id_pedido', $id_pedido, PDO::PARAM_INT);
      $cadastra->bindValue(':id_cupom', $id_cupom, PDO::PARAM_INT);
      $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $cadastra->execute();
    }

    public function createOrder($dados){
      $cadastra = $this->mysql->prepare('INSERT INTO `pedidos` (`referencia`, `id_cart`, `id_user`, `id_loja`, `valor`, `tipo_pag`, `troco`, `entrega`, `taxa_entrega`, `adiantado`, `fatura`, `status`, `pontos`, `pago`, `criado`)
                                          VALUES (:referencia, :id_cart, :id_user, :id_loja, :valor, :tipo_pag, :troco, :entrega, :taxa_entrega, :adiantado, :fatura, :status, :pontos, :pago, :criado);');
      $cadastra->bindValue(':referencia', $dados['ref'], PDO::PARAM_STR);
      $cadastra->bindValue(':id_cart', $dados['cid'], PDO::PARAM_INT);
      $cadastra->bindValue(':id_user', $dados['uid'], PDO::PARAM_INT);
      $cadastra->bindValue(':id_loja', $dados['lid'], PDO::PARAM_INT);
      $cadastra->bindValue(':valor', $dados['total'], PDO::PARAM_STR);
      $cadastra->bindValue(':tipo_pag', $dados['pagamento'], PDO::PARAM_INT);
      $cadastra->bindValue(':troco', $dados['troco'], PDO::PARAM_INT);
      $cadastra->bindValue(':entrega', $dados['entrega'], PDO::PARAM_INT);
      $cadastra->bindValue(':taxa_entrega', $dados['taxa_entrega'], PDO::PARAM_STR);
      $cadastra->bindValue(':adiantado', $dados['adiantado'], PDO::PARAM_STR);
      $cadastra->bindValue(':fatura', $dados['fatura'], PDO::PARAM_INT);
      $cadastra->bindValue(':status', $dados['status'], PDO::PARAM_INT);
      $cadastra->bindValue(':pontos', $dados['pontos'], PDO::PARAM_STR);
      $cadastra->bindValue(':pago', $dados['pago'], PDO::PARAM_STR);
      $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $cadastra->execute();
      $id_encomenda = $this->mysql->lastInsertId();

      $cadastra2 = $this->mysql->prepare('INSERT INTO `pedido_mudanca_status` (`id_pedido`, `tipo_mudanca`, `status`,`hora`) VALUES (:id_pedido, :tipo_mudanca, :status, :hora);');
      $cadastra2->bindValue(':id_pedido', $id_encomenda, PDO::PARAM_INT);
      $cadastra2->bindValue(':tipo_mudanca', 'cliente', PDO::PARAM_STR);
      $cadastra2->bindValue(':status', 1, PDO::PARAM_INT);
      $cadastra2->bindValue(':hora', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $cadastra2->execute();

      $update = $this->mysql->prepare('UPDATE `carrinho` SET `id_encomenda`= :id_encomenda, `id_cliente`= :id_cliente WHERE id = :id;');
      $update->bindValue(':id_encomenda', $id_encomenda, PDO::PARAM_INT);
      $update->bindValue(':id_cliente', $dados['uid'], PDO::PARAM_INT);
      $update->bindValue(':id', $dados['cid'], PDO::PARAM_INT);
      $update->execute();

      $this->cativaStockCarrinho($dados['cid']);

      $cadastra3 = $this->mysql->prepare('INSERT INTO `enderecos_comp` (`id_encomenda`, `freguesia`, `rua`, `cp`, `complemento`, `criado`) VALUES (:id_encomenda, :freguesia, :rua, :cp, :complemento, :criado);');
      $cadastra3->bindValue(':id_encomenda', $id_encomenda, PDO::PARAM_INT);
      $cadastra3->bindValue(':freguesia', $dados['freguesia'], PDO::PARAM_STR);
      $cadastra3->bindValue(':rua', $dados['rua'], PDO::PARAM_STR);
      $cadastra3->bindValue(':cp', $dados['cp'], PDO::PARAM_INT);
      $cadastra3->bindValue(':complemento', $dados['complemento'], PDO::PARAM_STR);
      $cadastra3->bindValue(':criado', date("Y-m-d"), PDO::PARAM_STR);
      $cadastra3->execute();
      return $id_encomenda;
    }

    public function cativaStockCarrinho($cid){
      $select = $this->mysql->prepare('SELECT id_produto, quantidade FROM `carrinho_item` WHERE `id_carrinho` = :cid');
      $select->bindValue(':cid', $cid, PDO::PARAM_INT);
      $select->execute();
      $cart_itens = $select->fetchAll(PDO::FETCH_ASSOC);

      foreach ($cart_itens as $item) {
        $this->cativaStockProduto($item['id_produto'], $item['quantidade']);
      }
    }

    public function cativaStockProduto($pid, $qty){
      $update = $this->mysql->prepare('UPDATE `produtos` SET `stock`= stock - :quantidade WHERE id = :pid');
      $update->bindValue(':quantidade', $qty, PDO::PARAM_INT);
      $update->bindValue(':pid', $pid, PDO::PARAM_INT);
      $update->execute();
    }

    public function getOrderById($oid){
      $select = $this->mysql->prepare('SELECT * FROM pedidos WHERE id = :id');
      $select->bindValue(':id', $oid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getOrderByRef($ref){
      $select = $this->mysql->prepare('SELECT * FROM pedidos WHERE referencia = :referencia');
      $select->bindValue(':referencia', $ref  , PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countOrdersLastMinute(){
      $select = $this->mysql->prepare('SELECT COUNT(*) as count FROM `pedidos` WHERE `criado` >= :init AND `criado` < :final');
      $select->bindValue(':init', date('Y-m-d H:i'), PDO::PARAM_STR);
      $select->bindValue(':final', date('Y-m-d H:i', strtotime('+1 minute')), PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function LastOrder(){
      $select = $this->mysql->prepare('SELECT id, referencia FROM pedidos ORDER BY id DESC LIMIT 1');
      $select->bindValue(':id', $oid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function buildOrderReference($getLojaMarca){

      $letras = range('A', 'Z');

      $grupo = 'IOR';
      $marca = $getLojaMarca['marca_codigo'];
      $pais = $getLojaMarca['pais'];
      $loja = sprintf('%03d', $getLojaMarca['codigo']);
      $data = date('dmy');
      $hora = date('Hi');

      $incomplete_order_ref = $grupo.$marca.$pais.$loja.$data.$hora;
      $last_orders = $this->countOrdersLastMinute()['count'];
      $letter_key = $last_orders == 0 ? 0 : ceil($last_orders / 100) - 1;

      if($last_orders % 100 == 0){
      	$letter_key = $last_orders >= 100 ? $letter_key +1 : $letter_key ;
      	$complete_order_ref = $incomplete_order_ref.$letras[$letter_key].'00';
        return $complete_order_ref;
      } else {
      	$position = sprintf('%02d', substr($last_orders, -2));
      	$complete_order_ref = $incomplete_order_ref.$letras[$letter_key].$position;
        return $complete_order_ref;
      }

    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO produtos_cat (nome) VALUES (:nome);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE produtos_cat SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
