<?php

class Lojas{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function lojaAberta($id_loja){
      $diasemana = array('dom', 'seg', 'ter', 'qua', 'qui', 'sex', 'sab');
      $diasemana_numero = date('w', strtotime(date('Y-m-d')));
      $week_day = $diasemana[$diasemana_numero];
      $hoje = $this->getLojaWorkDaysHours($id_loja, $week_day);

      if (empty($this->getLojaWorkDaysOff($id_loja))){ $fechado['dia_funcionamento'] = 1; } else { $fechado['dia_funcionamento'] = 0; }

      //if (time() >= strtotime($hoje['abertura']) && time() <= strtotime($hoje['fechamento'])){ $hora_funcionamento = 1; } else { $hora_funcionamento = 0; }
      foreach ($hoje as $horarios) {
        if (time() >= strtotime($horarios['abertura']) && time() <= strtotime($horarios['fechamento'])){ $hora_funcionamento[] = 1; } else { $hora_funcionamento[] = 0; }
      }
      if(in_array(1, $hora_funcionamento)){ $fechado['hora_funcionamento'] = 1; } else { $fechado['hora_funcionamento'] = 0; }

      //return $fechado;
      if(in_array(0, $fechado)){
        return false;
      } else {
        return true;
      }
    }

    public function getLojaMarca($lid){
      $select = $this->mysql->prepare('SELECT l.*, m.codigo as marca_codigo  FROM lojas l
                                        INNER JOIN marcas m ON l.marca_id = m.id
                                        WHERE l.id = :id');
      $select->bindValue(':id', $lid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function listarLojasMarca(){
      $select = $this->mysql->prepare('SELECT l.id, l.nome, l.slug, m.nome as marca, l.pais, l.codigo, l.tel, l.morada, l.descricao, l.ativo, l.codigo_postal FROM lojas l
                                        INNER JOIN marcas m ON l.marca_id = m.id WHERE l.ativo = 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function LojaMarcaById($id){
      $select = $this->mysql->prepare('SELECT l.id, l.nome, l.slug, m.nome as marca, l.pais, l.codigo, l.tel, l.morada, l.descricao, l.espera, l.taxa_entrega, l.entrega_gratis, l.codigo_postal FROM lojas l
                                        INNER JOIN marcas m ON l.marca_id = m.id WHERE l.id = :id');
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function LojaMarcaBySlug($slug){
      $select = $this->mysql->prepare('SELECT l.id, l.nome, l.slug, m.nome as marca, l.pais, l.codigo, l.tel, l.morada, l.descricao, l.espera, l.taxa_entrega, l.entrega_gratis, l.codigo_postal, l.ativo FROM lojas l
                                        INNER JOIN marcas m ON l.marca_id = m.id WHERE l.slug = :slug');
      $select->bindValue(':slug', $slug, PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getEndereco($cp){
      $select = $this->mysql->prepare('SELECT c.*, l.taxa_entrega as taxa_entrega_loja, l.slug FROM codigos_entrega c INNER JOIN lojas l ON c.id_loja = l.id WHERE c.codigo_postal = :codigo_postal AND c.ativo = 1');
      $select->bindValue(':codigo_postal', $cp, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getLojaWorkDaysHours($id, $dia_da_semana){
      $select = $this->mysql->prepare('SELECT * FROM `horario_atendimento` WHERE loja_id = :id AND dia_da_semana = :dia_da_semana AND ativo = 1');
      $select->bindValue(':dia_da_semana', $dia_da_semana, PDO::PARAM_STR);
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getLojaWorkDaysOff($id){
      $select = $this->mysql->prepare('SELECT d.data FROM day_off_lojas l INNER JOIN day_off d ON l.id_day = d.id WHERE l.id_loja = :id AND d.data = CURDATE() AND d.ativo = 1');
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getLojaEmenta($id){
      //$select = $this->mysql->prepare('SELECT l.*, p.* FROM produtos_loja l INNER JOIN produtos p ON l.id_produto = p.id WHERE l.id_loja = :id AND l.ativo = 1 AND p.ativo = 1');
      $select = $this->mysql->prepare('SELECT l.*, p.*, c.nome as cat FROM produtos_loja l
                                        INNER JOIN produtos p ON l.id_produto = p.id
                                        INNER JOIN produtos_cat c ON p.categoria = c.id
                                        WHERE l.id_loja = :id AND l.ativo = 1 AND p.ativo = 1');
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCategorias(){
      $select = $this->mysql->prepare('SELECT * FROM `produtos_cat` WHERE ativo = 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO lojas (nome) VALUES (:nome);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readLojas($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM lojas WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM lojas WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }else {
            $select = $this->mysql->prepare('SELECT * FROM lojas WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE lojas SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM lojas WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
