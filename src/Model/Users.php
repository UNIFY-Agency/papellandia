<?php
/**
 * Created by PhpStorm.
 * User: renan
 * Date: 07/09/15
 * Time: 19:17
 */

class Users{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function login($dados=null){

        //session_start();

        if(empty($dados)){
            if(!empty($_COOKIE['aaz_uryh']) and !empty($_COOKIE['aaz_oijd'])) {
                $dados['usuario'] = base64_decode($_COOKIE['aaz_uryh']);
                $dados['senha'] = base64_decode($_COOKIE['aaz_oijd']);
            }
        } else {

            $this->lembrar($dados);

        }

        if(isset($dados['usuario']) and isset($dados['senha'])) {

            $usuario = $this->retUsuario($dados['usuario']);
            if(!empty($usuario['email']) && empty($usuario['password'])){ return 'erro1'; }
            if (password_verify($dados['senha'], $usuario['password'])) {
                $_SESSION['usuario'] = $usuario;

                $update = $this->mysql->prepare('UPDATE usuarios SET logged = 1 WHERE id = :id;');
                $update->bindValue(':id', $usuario['id'], PDO::PARAM_INT);
                $update->execute();
                $_SESSION['usuario']['logged'] = 1;

            } else {
                return 'erro2';
            }
        }

    }

    public function force_login($dados=null){

        session_start();

        if(isset($dados['id'])) {
            $_SESSION['usuario'] = $dados;
            $update = $this->mysql->prepare('UPDATE usuarios SET logged = 1 WHERE id = :id;');
            $update->bindValue(':id', $dados['id'], PDO::PARAM_INT);
            $update->execute();
            $_SESSION['usuario']['logged'] = 1;
        }

    }

    public function setVerificaUsuario($id){

        $update = $this->mysql->prepare('UPDATE usuarios SET email_verificado = 1 WHERE id = :id;');
        $update->bindValue(':id', $id, PDO::PARAM_INT);
        return $update->execute();

    }

    public function changeUserPass($id, $senha){

        $update = $this->mysql->prepare('UPDATE usuarios SET password = :password WHERE id = :id;');
        $update->bindValue(':password', $this->hash($senha), PDO::PARAM_STR);
        $update->bindValue(':id', $id, PDO::PARAM_INT);
        return $update->execute();

    }

    public function logout(){

      session_start();

      $update = $this->mysql->prepare('UPDATE usuarios SET logged = 0 WHERE id = :id;');
      $update->bindValue(':id', $_SESSION['usuario']['id'], PDO::PARAM_INT);
      $update->execute();

      session_unset();
      session_destroy();
      setcookie('aaz_uryh');
      setcookie('aaz_oijd');
      // header("Location: URL_BASE");

    }

    public function protege(){

        session_start();
        if(empty($_SESSION['usuario']['logged'])){
            header('Location: ../');
        }

    }

    public function lembrar($dados){

        $cookie = array(
            'usuario'=>base64_encode($dados['usuario']),
            'senha'=>base64_encode($dados['senha'])
        );

        setcookie('aaz_uryh', $cookie['usuario'], (time() + (15*24*3600)), $_SERVER['SERVER_NAME']);
        setcookie('aaz_oijd', $cookie['senha'], (time() + (15*24*3600)), $_SERVER['SERVER_NAME']);

    }

    public function hash($senha){

        return password_hash($senha, PASSWORD_BCRYPT, array('cost'=>12));

    }

    public function retUsuario($user){

        $usuario = $this->mysql->prepare('SELECT * FROM usuarios WHERE email = :usuario');
        $usuario->bindValue(':usuario', $user, PDO::PARAM_STR);
        $usuario->execute();
        return $usuario->fetch(PDO::FETCH_ASSOC);

    }

    public function confirmUsuario($crypt){

        $usuario = $this->mysql->prepare('SELECT * FROM usuarios WHERE MD5(id) = :id_crypt');
        $usuario->bindValue(':id_crypt', $crypt, PDO::PARAM_STR);
        $usuario->execute();
        return $usuario->fetch(PDO::FETCH_ASSOC);

    }

    public function userBasicRegister($dados){
        $stmt = $this->mysql->prepare("SELECT * FROM usuarios WHERE email=:email");
        $stmt->bindValue(':email', $dados['email'], PDO::PARAM_STR);
        $stmt->execute();
        $user = $stmt->fetch(PDO::FETCH_ASSOC);
        if ($user) {
            if(!empty($user['email']) && empty($user['password'])){ return 'erro1'; } else { return 'erro2'; }
        } else {
            $cadastra = $this->mysql->prepare('INSERT into usuarios (`nome`, `email`, `nif`, `tel2`, `password`, `tipo`, `news`, `ativo`, `criado`) VALUES (:nome, :email, :nif, :tel2, :password, :tipo, :news, 1, :criado);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->bindValue(':email', $dados['email'], PDO::PARAM_STR);
            $cadastra->bindValue(':nif', $dados['nif'], PDO::PARAM_INT);
            $cadastra->bindValue(':tel2', $dados['tel2'], PDO::PARAM_STR);
            $cadastra->bindValue(':password', $this->hash($dados['password']), PDO::PARAM_STR);
            $cadastra->bindValue(':tipo', 0, PDO::PARAM_INT);
            $cadastra->bindValue(':news', $dados['news'], PDO::PARAM_INT);
            $cadastra->bindValue(':criado', date("Y-m-d"), PDO::PARAM_STR);
            $cadastra->execute();
            return $this->mysql->lastInsertId();
        }

    }

    public static function validateNIF($nif)
    {
        $nif = trim($nif);
        $nif_split = str_split($nif);
        $nif_primeiros_digito = array(1, 2, 3, 5, 6, 7, 8, 9);
        if (is_numeric($nif) && strlen($nif) == 9 && in_array($nif_split[0], $nif_primeiros_digito)) {
            $check_digit = 0;
            for ($i = 0; $i < 8; $i++) {
                $check_digit += $nif_split[$i] * (10 - $i - 1);
            }
            $check_digit = 11 - ($check_digit % 11);
            $check_digit = $check_digit >= 10 ? 0 : $check_digit;
            if ($check_digit == $nif_split[8]) {
                return true;
            }
        }
        return false;

    }

    public function cadastraNIF($id, $nif){
        $update = $this->mysql->prepare('UPDATE usuarios SET nif = :nif WHERE id = :id;');
        $update->bindValue(':nif', $nif, PDO::PARAM_INT);
        $update->bindValue(':id', $id, PDO::PARAM_INT);
        $update->execute();
    }

    //CRUD

    public function cadastrar($dados){

        $email = $dados[2];
        $stmt = $this->mysql->prepare("SELECT * FROM usuarios WHERE email=?");
        $stmt->execute([$email]);
        $user = $stmt->fetch();
        if ($user) {
          if(!empty($user['email']) && empty($user['password'])){ return 'erro1'; } else { return 'erro2'; }
        } else {
            $cadastra = $this->mysql->prepare('INSERT into usuarios (`nome`,`email`, `nif`, `tel2`, `password`, `tipo`, `morada`, `cp`, `localidade`, `distrito`, `id_pais`, `criado`)
                                                VALUES (:nome, :email, :nif, :tel2, :password, 0, :morada, :cp, :localidade, :distrito, :id_pais, :criado);');
            $cadastra->bindValue(':nome', $dados[0].' '.$dados[1], PDO::PARAM_STR);
            $cadastra->bindValue(':email', $dados[2], PDO::PARAM_STR);
            $cadastra->bindValue(':nif', preg_replace('/[^0-9]/', '', $dados[5]), PDO::PARAM_STR);
            $cadastra->bindValue(':tel2', preg_replace('/[^0-9]/', '', $dados[4]), PDO::PARAM_STR);
            $cadastra->bindValue(':password', $this->hash($dados[3]), PDO::PARAM_STR);
            $cadastra->bindValue(':morada', $dados[6], PDO::PARAM_STR);
            $cadastra->bindValue(':cp', preg_replace('/[^0-9]/', '', $dados[7]), PDO::PARAM_STR);
            $cadastra->bindValue(':localidade', $dados[8], PDO::PARAM_STR);
            $cadastra->bindValue(':distrito', $dados[9], PDO::PARAM_STR);
            $cadastra->bindValue(':id_pais', $dados[10], PDO::PARAM_INT);
            $cadastra->bindValue(':criado', date("Y-m-d"), PDO::PARAM_STR);
            //$cadastra->bindValue(':user_key', md5(substr($dados[0], 0, strpos($dados[0], '@'))).md5(substr($dados[2], -2)), PDO::PARAM_STR);
            $cadastra->execute();
            return $this->mysql->lastInsertId();
        }

    }

    public function readUsuario($nome=null, $id=null, $tipo=null, $admin=null){
        if(!empty($nome)) {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE nome = :nome;');
            $select->bindValue(':nome', $nome, PDO::PARAM_STR);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE id = :id;');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($tipo)) {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE tipo = :tipo;');
            $select->bindValue(':tipo', $tipo  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        } else if(!empty($admin)) {
            $select = $this->mysql->prepare('SELECT * FROM usuarios WHERE tipo = 0;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        } else {
            $select = $this->mysql->prepare('SELECT * FROM usuarios;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }

        $select->execute();
        return $select->fetch();
    }


    public function updateUsuario($dados, $id){
      if ($dados[3] == 0) {
        $dados[3] = $_SESSION['usuario']['password'];
      } else {
        $dados[3] = $this->hash($dados[3]);
      }
      $update = $this->mysql->prepare('UPDATE usuarios SET nome = :nome, email = :email, nif = :nif, tel2 = :tel2, password = :password, morada = :morada, cp = :cp, localidade = :localidade, distrito = :distrito, id_pais = :id_pais, atualizado = :atualizado WHERE id = :id;');
      $update->bindValue(':nome', $dados[0].' '.$dados[1], PDO::PARAM_STR);
      $update->bindValue(':email', $dados[2], PDO::PARAM_STR);
      $update->bindValue(':nif', preg_replace('/[^0-9]/', '', $dados[5]), PDO::PARAM_STR);
      $update->bindValue(':tel2', preg_replace('/[^0-9]/', '', $dados[4]), PDO::PARAM_STR);
      $update->bindValue(':password', $dados[3], PDO::PARAM_STR);
      $update->bindValue(':morada', $dados[6], PDO::PARAM_STR);
      $update->bindValue(':cp', preg_replace('/[^0-9]/', '', $dados[7]), PDO::PARAM_STR);
      $update->bindValue(':localidade', $dados[8], PDO::PARAM_STR);
      $update->bindValue(':distrito', $dados[9], PDO::PARAM_STR);
      $update->bindValue(':id_pais', $dados[10], PDO::PARAM_INT);
      $update->bindValue(':atualizado', date("Y-m-d"), PDO::PARAM_STR);
      $update->bindValue(':id', $id, PDO::PARAM_INT);
      return $update->execute();
    }

    public function deleteUsuario($id){
        $delete = $this->mysql->prepare('DELETE FROM usuarios WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }


}
