<?php

class Pedidos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function daysLastOrder($uid){
      $select = $this->mysql->prepare('SELECT id,DATEDIFF(CURRENT_DATE,criado) as dias FROM pedidos WHERE DATEDIFF(CURRENT_DATE,pedidos.criado) AND id_user = :id_user AND status <> 0 ORDER BY id DESC');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countUserProducts($uid){
      $select = $this->mysql->prepare('SELECT SUM(i.quantidade) as total FROM carrinho_item i
                                        INNER JOIN carrinho c ON i.id_carrinho = c.id
                                        INNER JOIN pedidos p ON c.id_encomenda = p.id
                                        WHERE c.id_cliente = :id_user AND p.status <> 0');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countUserOrders($uid){
      $select = $this->mysql->prepare('SELECT COUNT(*) as count FROM `pedidos` WHERE id_user = :id_user AND status <> 0');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getUserOrders($uid){
      $select = $this->mysql->prepare('SELECT * FROM `pedidos` WHERE id_user = :id_user');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function sumUserPoints($uid){
      $select = $this->mysql->prepare('SELECT SUM(p.pontos) as points FROM `pedidos` p WHERE id_user = :id_user  AND p.status <> 0');
      $select->bindValue(':id_user', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getOrdersDetails($pid, $lg){
      $select = $this->mysql->prepare("SELECT i.id, p.referencia as encomenda_ref, p.status, i.id_produto, e_nome.$lg as product_name, pr.referencia as product_ref, pav.valor as product_size, json_extract(pr.preco, '$.$lg') as preco, i.quantidade, pr.grupo, u.nome as user_name, u.tel, u.tel2, u.email,
                                      u.nif, p.fatura, p.tipo_pag, p.troco, p.entrega, p.taxa_entrega, p.adiantado, p.valor as total_pedido, c.alergias, p.pontos, pr.image_url,
                                     (SELECT concat(freguesia, ' - ', rua, ' ', cp, ' ', complemento) FROM enderecos_comp WHERE id_encomenda = p.id ) as endereco_comp, 0 as combo FROM carrinho_item i
                                        INNER JOIN produtos pr ON i.id_produto = pr.id
                                        INNER JOIN carrinho c ON i.id_carrinho = c.id
                                        #INNER JOIN codigos_entrega e ON c.id_cp = e.id
                                        #INNER JOIN lojas l ON c.id_loja = l.id
                                        INNER JOIN pedidos p ON c.id_encomenda = p.id
                                        INNER JOIN usuarios u ON p.id_user = u.id
                                        INNER JOIN exp e_nome ON pr.nome = e_nome.id
                                        INNER JOIN produtos_variacoes pv ON pv.id_filho = pr.id
                                        INNER JOIN produtos_multivariacoes pmu ON pv.id = pmu.id_pai
                                        INNER JOIN produtos_atributos_valores pav ON pmu.id_filho = pav.id
                                        WHERE i.removido = 0 AND p.id = :pid");
      $select->bindValue(':pid', $pid, PDO::PARAM_INT);
      $select->execute();
      //return $select->fetchAll(PDO::FETCH_ASSOC);
      $cart = $select->fetchAll(PDO::FETCH_ASSOC);
      foreach ($cart as $cart_item) {
        $final_cart[] = $cart_item;
        if($cart_item['grupo'] == 2){
          $c = 0;
          $combo_itens = $this->getCartGroupItens($cart_item['id']);
          foreach ($combo_itens as $cart_group_item) {
          $final_cart[] = $cart_group_item;
          $c++;
          }
        }
      }
      return $final_cart;
    }

    public function createOrder($dados){
      $cadastra = $this->mysql->prepare('INSERT INTO `pedidos` (`referencia`, `id_cart`, `id_user`, `valor`, `status`, `pontos`, `criado`) VALUES (:referencia, :id_cart, :id_user, :valor, :status, :pontos, :criado);');
      $cadastra->bindValue(':referencia', $dados['ref'], PDO::PARAM_STR);
      $cadastra->bindValue(':id_cart', $dados['cid'], PDO::PARAM_INT);
      $cadastra->bindValue(':id_user', $dados['uid'], PDO::PARAM_INT);
      $cadastra->bindValue(':valor', $dados['total'], PDO::PARAM_STR);
      $cadastra->bindValue(':status', $dados['status'], PDO::PARAM_INT);
      $cadastra->bindValue(':pontos', $dados['pontos'], PDO::PARAM_STR);
      $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $cadastra->execute();
      $id_encomenda = $this->mysql->lastInsertId();

      $update = $this->mysql->prepare('UPDATE `carrinho` SET `id_encomenda`= :id_encomenda WHERE id = :id;');
      $update->bindValue(':id_encomenda', $id_encomenda, PDO::PARAM_INT);
      $update->bindValue(':id', $dados['cid'], PDO::PARAM_INT);
      $update->execute();

      $cadastra2 = $this->mysql->prepare('INSERT INTO `enderecos_comp` (`id_encomenda`, `rua`, `numero`, `complemento`, `criado`) VALUES (:id_encomenda, :rua, :numero, :complemento, :criado);');
      $cadastra2->bindValue(':id_encomenda', $id_encomenda, PDO::PARAM_INT);
      $cadastra2->bindValue(':rua', $dados['rua'], PDO::PARAM_STR);
      $cadastra2->bindValue(':numero', $dados['numero'], PDO::PARAM_INT);
      $cadastra2->bindValue(':complemento', $dados['complemento'], PDO::PARAM_STR);
      $cadastra2->bindValue(':criado', date("Y-m-d"), PDO::PARAM_STR);
      $cadastra2->execute();
      return $id_encomenda;
    }

    public function getOrderById($oid){
      $select = $this->mysql->prepare('SELECT * FROM pedidos WHERE id = :id');
      $select->bindValue(':id', $oid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getOrderByRef($ref){
      $select = $this->mysql->prepare('SELECT * FROM pedidos WHERE referencia = :referencia');
      $select->bindValue(':referencia', $ref  , PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function countOrdersLastMinute(){
      $select = $this->mysql->prepare('SELECT COUNT(*) as count FROM `pedidos` WHERE `criado` >= :init AND `criado` < :final');
      $select->bindValue(':init', date('Y-m-d H:i'), PDO::PARAM_STR);
      $select->bindValue(':final', date('Y-m-d H:i', strtotime('+1 minute')), PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function LastOrder(){
      $select = $this->mysql->prepare('SELECT id, referencia FROM pedidos ORDER BY id DESC LIMIT 1');
      $select->bindValue(':id', $oid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function buildOrderReference($getLojaMarca){

      $letras = range('A', 'Z');

      $grupo = 'R';
      $marca = $getLojaMarca['marca_codigo'];
      $pais = $getLojaMarca['pais'];
      $loja = sprintf('%03d', $getLojaMarca['codigo']);
      $data = date('dmy');
      $hora = date('Hi');

      $incomplete_order_ref = $grupo.$marca.$pais.$loja.$data.$hora;
      $last_orders = $this->countOrdersLastMinute()['count'];
      $letter_key = $last_orders == 0 ? 0 : ceil($last_orders / 100) - 1;

      if($last_orders % 100 == 0){
      	$letter_key = $last_orders >= 100 ? $letter_key +1 : $letter_key ;
      	$complete_order_ref = $incomplete_order_ref.$letras[$letter_key].'00';
        return $complete_order_ref;
      } else {
      	$position = sprintf('%02d', substr($last_orders, -2));
      	$complete_order_ref = $incomplete_order_ref.$letras[$letter_key].$position;
        return $complete_order_ref;
      }

    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO produtos_cat (nome) VALUES (:nome);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE produtos_cat SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
