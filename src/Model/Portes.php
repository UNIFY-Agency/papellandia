<?php

class Portes{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //Essa classe opera todas as tabelas portes tendo uma regra específica de inteiros para cada situaçãoptimize

    // TABELA portes_regras ---------------
    //operador 0 = -
    //operador 1 = =
    //operador 2 = +

    //modificador 0 = -
    //modificador 1 = +


    public function getPorteRules($pid){
      $select = $this->mysql->prepare('SELECT * FROM portes_regras_paises prp INNER JOIN portes_regras pr ON prp.id_regra = pr.id WHERE prp.id_pais = :id_pais AND pr.ativo = 1');
      $select->bindValue(':id_pais', $pid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getSendMethods(){
      $select = $this->mysql->prepare('SELECT * FROM `send_mothods`');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function getProductsIdsByCategoriesIntersection($slugs){

      $slugs = "'".implode("','", $slugs)."'";
      $select = $this->mysql->prepare("SELECT * FROM categorias WHERE slug IN($slugs)");
      $select->execute();
      $cat_ids = array_column($select->fetchAll(PDO::FETCH_ASSOC), 'id');

      $categorias_ids = implode(',', $cat_ids);
      $select = $this->mysql->prepare("SELECT * FROM `produtos_categoria` WHERE id_categoria IN($categorias_ids)");
      $select->execute();
      $registros = $select->fetchAll(PDO::FETCH_ASSOC);

      $prod_ids = array_column($registros, 'id_produto');
      $prod_quant = array_count_values($prod_ids);

      $prod_id_filtered = array();
      foreach ($prod_ids as $key => $value) {
        if($prod_quant[$value] >= count($cat_ids)){
          array_push($prod_id_filtered, $value);
        }
      }

      // echo '<pre>';
      // print_r($prod_id_filtered);
      // echo '</pre>';
      // die();

      return array_unique($prod_id_filtered);
    }

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO produtos_cat (nome, slug, ativo) VALUES (:nome, :slug, 1);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->bindValue(':slug', Resources::slug($dados['nome']), PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE produtos_cat SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
