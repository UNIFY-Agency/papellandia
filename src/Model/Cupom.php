<?php

class Cupom{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    //tipo 1 = desconto porcentagem
    //tipo 2 = desconto dinheiro
    //tipo 3 = insere produto no carrinho

    //user_control 1 = cupom sem limitação no ambito de usuario
    //user_control 2 = cupom so pode ser usado uma vez por usuario
    //user_control 3 = cupom só pode ser utilizado por usuários que nunca fizeram pedidos

    public function getCupom($codigo, $session){

      $id_loja = empty($session['carrinho']['id_loja']) ? 1 : $session['carrinho']['id_loja'] ;
      $id_user = $session['usuario']['id'];

      $select = $this->mysql->prepare('SELECT * FROM cupom WHERE codigo = :codigo AND ativo = 1');
      $select->bindValue(':codigo', $codigo, PDO::PARAM_STR);
      $select->execute();
      $cupom = $select->fetch(PDO::FETCH_ASSOC);

      if (!empty($cupom)) {
        $select2 = $this->mysql->prepare('SELECT * FROM cupom_loja WHERE id_cupom = :id_cupom AND id_loja = :id_loja AND ativo = 1');
        $select2->bindValue(':id_cupom', $cupom['id'], PDO::PARAM_INT);
        $select2->bindValue(':id_loja', $id_loja  , PDO::PARAM_INT);
        $select2->execute();
        $cupom_loja = $select2->fetch(PDO::FETCH_ASSOC);
      }

      if ($cupom['limitado'] == 1) {
        $select3 = $this->mysql->prepare('SELECT COUNT(id_cupom) as quant FROM pedidos_cupom WHERE id_cupom = :id_cupom');
        $select3->bindValue(':id_cupom', $cupom['id'], PDO::PARAM_INT);
        $select3->execute();
        $cupom_uses = $select3->fetch(PDO::FETCH_ASSOC)['quant'];
      }

      if ($cupom['user_control'] == 2) {
        $select4 = $this->mysql->prepare('SELECT COUNT(pc.id) as quant FROM pedidos_cupom pc INNER JOIN pedidos p ON pc.id_pedido = p.id WHERE pc.id_cupom = :id_cupom AND p.id_user = :id_user');
        $select4->bindValue(':id_cupom', $cupom['id'], PDO::PARAM_INT);
        $select4->bindValue(':id_user', $id_user, PDO::PARAM_INT);
        $select4->execute();
        $cupom_sameuser = $select4->fetch(PDO::FETCH_ASSOC)['quant'];
      }

      if ($cupom['user_control'] == 3) {
        $select5 = $this->mysql->prepare('SELECT COUNT(id) as quant FROM `pedidos` WHERE id_user = :id_user');
        $select5->bindValue(':id_user', $id_user, PDO::PARAM_INT);
        $select5->execute();
        $cupom_userpedidos = $select5->fetch(PDO::FETCH_ASSOC)['quant'];
      }

      // if (empty($cupom_loja)) {
      //   return 'erro2';
      // } else {
      //   return $cupom;
      // }

      switch (true) {
        case empty($cupom):
          return 'erro1';
          break;
        case empty($cupom_loja):
          return 'erro2';
          break;
        case ($cupom['user_control'] > 1 && empty($session['usuario']['email'])): //usuario deslogado
          return 'erro3';
          break;
        case ($cupom['limitado'] == 1 && $cupom_uses >= $cupom['limite']):
          return 'erro4';
          break;
        case ($cupom['user_control'] == 2 && $cupom_sameuser >= 1):
          return 'erro5';
          break;
        case ($cupom['user_control'] == 3 && $cupom_userpedidos > 0):
          return 'erro6';
          break;
        default:
          return $cupom;
      }
    }

    public function useCupom($id){
      $atualiza = $this->mysql->prepare('UPDATE cupom SET ativo=0, atualizado=:atualizado WHERE id = :id ');
      $atualiza->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $atualiza->bindValue(':id', $id, PDO::PARAM_INT);
      $atualiza->execute();
    }

    public function saveCupom($id){
      $atualiza = $this->mysql->prepare('UPDATE cupom SET ativo=1, atualizado=:atualizado WHERE id = :id ');
      $atualiza->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $atualiza->bindValue(':id', $id, PDO::PARAM_INT);
      $atualiza->execute();
    }

    //CRUD

    public function insertCupom($dados){

      $dado = $dados['codigo'];
      $consulta = $this->mysql->prepare("SELECT * FROM cupom WHERE codigo=?");
      $consulta->execute([$dado]);
      $registro = $consulta->fetch();
      if ($registro) {
          return 'erro';
      } else {
        $cadastra = $this->mysql->prepare('INSERT INTO cupom (nome, codigo, tipo, valor, criado) VALUES (:nome, :codigo, :tipo, :valor, :criado);');
        $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $cadastra->bindValue(':codigo', $dados['codigo'], PDO::PARAM_STR);
        $cadastra->bindValue(':tipo', $dados['tipo'], PDO::PARAM_INT);
        $cadastra->bindValue(':valor', $dados['valor'], PDO::PARAM_INT);
        $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $cadastra->execute();
      }
    }

    public function readCupom($id=null, $codigo=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM cupom WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($codigo)) {
            $select = $this->mysql->prepare('SELECT * FROM cupom WHERE codigo = :codigo');
            $select->bindValue(':codigo', $codigo  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        }else {
            $select = $this->mysql->prepare('SELECT * FROM cupom WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCupom($dados, $id){
      $consulta = $this->mysql->prepare("SELECT * FROM cupom WHERE codigo=:codigo AND id <> :id");
      $consulta->bindValue(':codigo', $dados['codigo'], PDO::PARAM_STR);
      $consulta->bindValue(':id', $id, PDO::PARAM_INT);
      $consulta->execute();
      $registro = $consulta->fetch();
      if ($registro) {
          return 'erro';
      } else {
        $atualiza = $this->mysql->prepare('UPDATE cupom SET nome=:nome, codigo=:codigo, tipo=:tipo, valor=:valor, atualizado=:atualizado WHERE id = :id ');
        $atualiza->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $atualiza->bindValue(':codigo', $dados['codigo'], PDO::PARAM_STR);
        $atualiza->bindValue(':tipo', $dados['tipo'], PDO::PARAM_INT);
        $atualiza->bindValue(':valor', $dados['valor'], PDO::PARAM_INT);
        $atualiza->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $atualiza->bindValue(':id', $id, PDO::PARAM_INT);
        $atualiza->execute();
      }
    }

    public function deleteCupom($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }

}
