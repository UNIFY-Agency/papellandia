<?php

class Wishlist{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function getMyWishlist($uid, $lg){
      $select = $this->mysql->prepare("SELECT w.id as id_lista, p.id, p.referencia, json_extract(p.preco, '$.$lg') as preco, e_nome.$lg as nome, e_desc.$lg as descricao, e_comp.$lg as composicao, image_url FROM `wishlist` w 
                                        INNER JOIN produtos p ON w.id_prod = p.id
                                        INNER JOIN produtos_cat pc ON p.categoria = pc.id
                                        INNER JOIN exp e_nome ON p.nome = e_nome.id
                                        INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                        INNER JOIN exp e_comp ON p.composicao = e_comp.id
                                        WHERE w.id_user = :id");
      $select->bindValue(':id', $uid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function insertItem($dados){
      $cadastra = $this->mysql->prepare('INSERT INTO `wishlist`(`id_user`, `id_prod`, `criado`) VALUES (:id_user, :id_prod, :criado);');
      $cadastra->bindValue(':id_user', $dados['id_user'], PDO::PARAM_INT);
      $cadastra->bindValue(':id_prod', $dados['id_prod'], PDO::PARAM_INT);
      $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $cadastra->execute();
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM wishlist WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM wishlist WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM wishlist WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE wishlist SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM wishlist WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
