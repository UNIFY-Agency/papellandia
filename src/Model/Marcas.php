<?php

class Marcas{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function slug($title){
        $slug = strtolower($title);
        $slug = preg_replace('/[^a-z0-9]\ -/', '', $slug);
        $slug = preg_replace('/[ ]\ -/', '-', $slug);
        return $slug;
    }

    public function getMarcaPais($pais){
      $consulta = $this->mysql->prepare("SELECT m.*, paises.codigo, p.ativo FROM marcas_pais p
                                          INNER JOIN marcas m ON p.id_marca = m.id
                                          INNER JOIN paises ON p.pais = paises.id
                                          WHERE paises.ativo = 1 AND paises.codigo = '$pais' ;");
      $consulta->execute();
      return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function cadastrarMarcaPais($marca, $pais, $status){

      $consulta = $this->mysql->prepare("SELECT * FROM marcas_pais WHERE id_marca=:id_marca AND pais=:pais;");
      $consulta->bindValue(':id_marca', $marca, PDO::PARAM_INT);
      $consulta->bindValue(':pais', $pais, PDO::PARAM_STR);
      $consulta->execute();
      $marca_pais = $consulta->fetch(PDO::FETCH_ASSOC);
      if ($marca_pais) {
        $this::atualizaMarcaPais($marca, $pais, $status);
      } else {
        $cadastra = $this->mysql->prepare('INSERT INTO marcas_pais (id_marca, pais, ativo, criado) VALUES (:id_marca, :pais, :ativo, :criado);');
        $cadastra->bindValue(':id_marca', $marca, PDO::PARAM_INT);
        $cadastra->bindValue(':pais', $pais, PDO::PARAM_STR);
        $cadastra->bindValue(':ativo', $status, PDO::PARAM_INT);
        $cadastra->bindValue(':criado', date('Y-m-d'), PDO::PARAM_STR);
        $cadastra->execute();
      }
    }

    public function atualizaMarcaPais($marca, $pais, $status){
      $consulta = $this->mysql->prepare("SELECT * FROM marcas_pais WHERE id_marca=:id_marca AND pais=:pais;");
      $consulta->bindValue(':id_marca', $marca, PDO::PARAM_INT);
      $consulta->bindValue(':pais', $pais, PDO::PARAM_STR);
      $consulta->execute();
      $marca_pais = $consulta->fetch(PDO::FETCH_ASSOC);
      if ($marca_pais) {
        $atualiza = $this->mysql->prepare('UPDATE marcas_pais SET ativo = :ativo, atualizado = :atualizado WHERE id_marca = :id AND pais = :pais;');
        $atualiza->bindValue(':pais', $pais, PDO::PARAM_STR);
        $atualiza->bindValue(':ativo', $status, PDO::PARAM_INT);
        $atualiza->bindValue(':atualizado', date('Y-m-d'), PDO::PARAM_STR);
        $atualiza->bindValue(':id', $marca, PDO::PARAM_INT);
        $atualiza->execute();
      } else {
        $this::cadastrarMarcaPais($marca, $pais, $status);
      }

    }

    public function getPaisesMarca($id){
      $consulta = $this->mysql->prepare("SELECT pais FROM marcas_pais WHERE id_marca=:id_marca AND ativo = 1;");
      $consulta->bindValue(':id_marca', $id, PDO::PARAM_INT);
      $consulta->execute();
      return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getMarcasName(){
      $consulta = $this->mysql->prepare("SELECT id, nome FROM marcas WHERE 1;");
      $consulta->execute();
      return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    /*SELECT marcas.id, marcas.nome, marcas.segmento, marcas.codigo, marcas_pais.pais, marcas.criado FROM `marcas_pais`
INNER JOIN marcas ON marcas_pais.id_marca = marcas.id
WHERE marcas_pais.ativo = 1*/

    public function listarMarcasPaises(){
      $select = $this->mysql->prepare('SELECT marcas.id, marcas.nome, marcas.segmento, marcas.codigo, paises.codigo as pais, marcas_pais.ativo, marcas.criado FROM `marcas_pais`
                                        INNER JOIN marcas ON marcas_pais.id_marca = marcas.id
                                        INNER JOIN paises ON marcas_pais.pais = paises.id');
      $select->execute();
      $marcas_paises = $select->fetchAll(PDO::FETCH_ASSOC);
      if($marcas_paises){
        return $marcas_paises;
      } else {
        $select = $this->mysql->prepare('SELECT * FROM `marcas` WHERE 1');
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      }
    }

    public function listarMarcaPaises($id){
      $select = $this->mysql->prepare('SELECT marcas.id, marcas.nome, marcas.segmento, marcas.codigo, marcas.descricao, marcas_pais.pais, marcas_pais.ativo, marcas.criado FROM `marcas_pais`
                                        INNER JOIN marcas ON marcas_pais.id_marca = marcas.id
                                        WHERE marcas.id = :id');
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getPaisesAtivos(){
      $select = $this->mysql->prepare('SELECT * FROM `paises` WHERE ativo = 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function insertMarca($dados){

      $codigo = $dados['codigo'];
      $consulta = $this->mysql->prepare("SELECT * FROM marcas WHERE codigo=?");
      $consulta->execute([$codigo]);
      $marca = $consulta->fetch();
      if ($marca) {
          return 'erro';
      } else {
        $cadastra = $this->mysql->prepare('INSERT INTO marcas (nome, segmento, codigo, descricao, criado) VALUES (:nome, :segmento, :codigo, :descricao, :criado);');
        $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $cadastra->bindValue(':segmento', $dados['segmento'], PDO::PARAM_STR);
        $cadastra->bindValue(':codigo', $dados['codigo'], PDO::PARAM_STR);
        $cadastra->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
        $cadastra->bindValue(':criado', date('Y-m-d'), PDO::PARAM_STR);
        $cadastra->execute();
        return $this->mysql->lastInsertId();
      }
    }

    public function readMarcas($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM marcas WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM marcas WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM marcas WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editMarca($id, $dados){

      $codigo = $dados['codigo'];
      $consulta = $this->mysql->prepare("SELECT * FROM marcas WHERE codigo=? AND id <> $id");
      $consulta->execute([$codigo]);
      $marca = $consulta->fetch();
      if ($marca) {
          return 'erro';
      } else {
        $atualiza = $this->mysql->prepare('UPDATE marcas SET nome = :nome, segmento = :segmento, codigo = :codigo, descricao = :descricao, atualizado = :atualizado WHERE id = :id ');
        $atualiza->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $atualiza->bindValue(':segmento', $dados['segmento'], PDO::PARAM_STR);
        $atualiza->bindValue(':codigo', $dados['codigo'], PDO::PARAM_STR);
        $atualiza->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
        $atualiza->bindValue(':atualizado', date('Y-m-d'), PDO::PARAM_STR);
        $atualiza->bindValue(':id', $id, PDO::PARAM_INT);
        $atualiza->execute();
      }
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM marcas WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
