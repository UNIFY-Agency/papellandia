<?php

class Cart{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function setCartAllergyes($cid, $allergies){
      $update = $this->mysql->prepare('UPDATE `carrinho` SET `alergias`= :alergias WHERE id = :cid;');
      $update->bindValue(':alergias', $allergies, PDO::PARAM_STR);
      $update->bindValue(':cid', $cid, PDO::PARAM_INT);
      $update->execute();
    }

    public function purgeCart($cid){
      $update = $this->mysql->prepare('UPDATE `carrinho` SET `removido`= 1 WHERE id = :id;');
      $update->bindValue(':id', $cid, PDO::PARAM_INT);
      $update->execute();
    }

    public function changeCartMaster($id_cliente, $cid){
      $update = $this->mysql->prepare('UPDATE `carrinho` SET `id_cliente`= :id_cliente WHERE id = :id AND removido = 0');
      $update->bindValue(':id_cliente', $id_cliente, PDO::PARAM_INT);
      $update->bindValue(':id', $cid, PDO::PARAM_INT);
      $update->execute();
    }

    // public function AlterCartLineQty($mod, $lid){
    //   $consulta = $this->mysql->prepare("SELECT * FROM carrinho_item WHERE `id` = :lid");
    //   $consulta->bindValue(':lid', $lid, PDO::PARAM_INT);
    //   $consulta->execute();
    //   $registro = $consulta->fetch(PDO::FETCH_ASSOC);
    //
    //   if($mod == 'plus'){
    //     $quantidade = $registro['quantidade'] + 1 ;
    //     if(!is_null($registro['max']) && $quantidade > $registro['max']){ return 'erro1'; }
    //   } else if($mod == 'minus'){
    //     $quantidade = $registro['quantidade'] - 1 ;
    //     if(!is_null($registro['min']) && $quantidade > $registro['min']){ return 'erro2'; }
    //   } else { return false; }
    //
    //   $update = $this->mysql->prepare('UPDATE `carrinho_item` SET `quantidade`= :quantidade WHERE id = :lid;');
    //   $update->bindValue(':quantidade', $quantidade, PDO::PARAM_INT);
    //   $update->bindValue(':lid', $lid, PDO::PARAM_INT);
    //   $update->execute();
    // }

    public function AlterCartLineQty($qty, $lid){
      $consulta = $this->mysql->prepare("SELECT * FROM `carrinho_item` WHERE id = :lid");
      $consulta->bindValue(':lid', $lid, PDO::PARAM_INT);
      $consulta->execute();
      $line = $consulta->fetch(PDO::FETCH_ASSOC);

      $prod_qty = $this->getProdQty($line['id_produto']);

      if ($prod_qty >= $qty) {
        $update = $this->mysql->prepare('UPDATE `carrinho_item` SET `quantidade`= :quantidade WHERE id = :lid;');
        $update->bindValue(':quantidade', $qty, PDO::PARAM_INT);
        $update->bindValue(':lid', $lid, PDO::PARAM_INT);
        $update->execute();
      } else {
        return 'erro0';
      }
    }

    public function getProdQty($pid){
      $consulta2 = $this->mysql->prepare("SELECT stock FROM `produtos` WHERE id = :pid");
      $consulta2->bindValue(':pid', $pid, PDO::PARAM_INT);
      $consulta2->execute();
      $prod = $consulta2->fetch(PDO::FETCH_ASSOC);
      return $prod['stock'];
    }

    public function removeCartItem($lid){
      $consulta = $this->mysql->prepare("SELECT i.*, p.removivel FROM carrinho_item i
                                        INNER JOIN produtos_action p ON i.id_produto = p.id_produto
                                        WHERE i.id = :lid");
      $consulta->bindValue(':lid', $lid, PDO::PARAM_INT);
      $consulta->execute();
      $registro = $consulta->fetch(PDO::FETCH_ASSOC);

      if($registro['removivel'] == 0 && !empty($registro)){
        return 'erro1';
      }

      $update = $this->mysql->prepare('UPDATE `carrinho_item` SET `removido`= 1 WHERE id = :id;');
      $update->bindValue(':id', $lid, PDO::PARAM_INT);
      $update->execute();
    }

    public function refleshCart($uid, $lg=null){
      $carrinho = $this->getCart($uid);
      if ($lg) {
        if($carrinho){
          $_SESSION['carrinho'] = $carrinho;
          $_SESSION['carrinho']['itens'] = $this->getCartItens($uid, $carrinho['id'], $lg);
          $total = $this->getCartTotals($uid, $carrinho['id'], $lg);
          $_SESSION['carrinho']['total']['quantidade'] = empty($total['quantidade']) ? 0 : $total['quantidade'] ;
          $_SESSION['carrinho']['total']['total'] = empty($total['total']) ? 0 : $total['total'] ;
        } else {
          unset($_SESSION['carrinho']);
        }
      } else {
        if($carrinho){
          $_SESSION['carrinho'] = $carrinho;
          $_SESSION['carrinho']['itens'] = $this->getCartItens($uid, $carrinho['id']);
          $total = $this->getCartTotals($uid, $carrinho['id']);
          $_SESSION['carrinho']['total']['quantidade'] = empty($total['quantidade']) ? 0 : $total['quantidade'] ;
          $_SESSION['carrinho']['total']['total'] = empty($total['total']) ? 0 : $total['total'] ;
        } else {
          unset($_SESSION['carrinho']);
        }
      }
    }

    public function getCart($uid){
      $select = $this->mysql->prepare('SELECT * FROM `carrinho` WHERE `id_cliente` = :id_cliente AND id_encomenda is NULL AND removido = 0;');
      $select->bindValue(':id_cliente', $uid, PDO::PARAM_STR);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getCartItens($uid, $cid, $lg=null){
      if ($lg) {
        $select = $this->mysql->prepare('SELECT i.id, i.id_produto, p.grupo, e_nome.'.$lg.' as nome, p.descricao, REPLACE(json_extract(p.preco, "$.'.$lg.'"),\'"\',\'\') as preco, i.quantidade, c.id_loja, i.id_carrinho,
                                          (SELECT image_url FROM produtos WHERE referencia = p.referencia AND tipo = 1 LIMIT 1) as image_url FROM carrinho_item i
                                          INNER JOIN carrinho c ON i.id_carrinho = c.id INNER JOIN produtos p ON i.id_produto = p.id
                                          INNER JOIN exp e_nome ON p.nome = e_nome.id
                                          WHERE c.id_cliente = :id_cliente AND i.id_carrinho = :id_carrinho AND i.removido = 0 AND c.id_encomenda is NULL AND c.removido = 0;');
        $select->bindValue(':id_cliente', $uid, PDO::PARAM_STR);
        $select->bindValue(':id_carrinho', $cid, PDO::PARAM_STR);
        $select->execute();
        //return $select->fetchAll(PDO::FETCH_ASSOC);
        $cart = $select->fetchAll(PDO::FETCH_ASSOC);
        foreach ($cart as $cart_item) {
          $final_cart[$cart_item['id']] = $cart_item;
          if($cart_item['grupo'] == 2){
            $final_cart[$cart_item['id']]['group_itens'] = $this->getCartGroupItens($cart_item['id']);
          }
        }
        return $final_cart;
      } else {
        $select = $this->mysql->prepare('SELECT i.id, i.id_produto, p.grupo, p.nome, p.descricao, p.preco, i.quantidade, p.image_url, c.id_loja, i.id_carrinho FROM carrinho_item i
                                          INNER JOIN carrinho c ON i.id_carrinho = c.id INNER JOIN produtos p ON i.id_produto = p.id
                                          WHERE c.id_cliente = :id_cliente AND i.id_carrinho = :id_carrinho AND i.removido = 0 AND c.id_encomenda is NULL AND c.removido = 0;');
        $select->bindValue(':id_cliente', $uid, PDO::PARAM_STR);
        $select->bindValue(':id_carrinho', $cid, PDO::PARAM_STR);
        $select->execute();
        //return $select->fetchAll(PDO::FETCH_ASSOC);
        $cart = $select->fetchAll(PDO::FETCH_ASSOC);
        foreach ($cart as $cart_item) {
          $final_cart[$cart_item['id']] = $cart_item;
          if($cart_item['grupo'] == 2){
            $final_cart[$cart_item['id']]['group_itens'] = $this->getCartGroupItens($cart_item['id']);
          }
        }
        return $final_cart;
      }
    }

    public function getMiniCartItens($uid, $cid){
      // o produto id 1 está sendo ignorado porque se trata do kit gratuito de hashi
      $select = $this->mysql->prepare('SELECT i.id, i.id_produto, p.grupo, p.nome, p.preco, i.quantidade, p.image_url, c.id_loja, i.id_carrinho FROM carrinho_item i
                                        INNER JOIN carrinho c ON i.id_carrinho = c.id INNER JOIN produtos p ON i.id_produto = p.id
                                        WHERE c.id_cliente = :id_cliente AND i.id_carrinho = :id_carrinho AND i.removido = 0 AND c.id_encomenda is NULL AND c.removido = 0 AND p.id <> 1;');
      $select->bindValue(':id_cliente', $uid, PDO::PARAM_STR);
      $select->bindValue(':id_carrinho', $cid, PDO::PARAM_STR);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCartGroupItens($lid){
      $select = $this->mysql->prepare('SELECT cgi.id_produto, cgi.quantidade, ci.id_produto as combo_id, p.nome, p.categoria
                                      FROM carrinho_group_item cgi
                                      INNER JOIN carrinho_item ci ON cgi.id_linha = ci.id
                                      INNER JOIN produtos p ON cgi.id_produto = p.id
                                      WHERE cgi.id_linha = :id_line');
      $select->bindValue(':id_line', $lid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getCartTotals($uid, $cid, $lg=null){
      if ($lg) {
        $select = $this->mysql->prepare('SELECT SUM(i.quantidade) AS quantidade, SUM(REPLACE(json_extract(p.preco, "$.'.$lg.'"),\'"\',\'\') * i.quantidade) AS total FROM carrinho_item i
                                          INNER JOIN carrinho c ON i.id_carrinho = c.id INNER JOIN produtos p ON i.id_produto = p.id
                                          WHERE c.id_cliente = :id_cliente AND i.id_carrinho = :id_carrinho AND i.removido = 0 AND c.id_encomenda is NULL AND c.removido = 0');
        $select->bindValue(':id_cliente', $uid, PDO::PARAM_STR);
        $select->bindValue(':id_carrinho', $cid, PDO::PARAM_STR);
        $select->execute();
        return $select->fetch(PDO::FETCH_ASSOC);
      } else {
        $select = $this->mysql->prepare('SELECT SUM(i.quantidade) AS quantidade, SUM(p.preco * i.quantidade) AS total FROM carrinho_item i
                                          INNER JOIN carrinho c ON i.id_carrinho = c.id INNER JOIN produtos p ON i.id_produto = p.id
                                          WHERE c.id_cliente = :id_cliente AND i.id_carrinho = :id_carrinho AND i.removido = 0 AND c.id_encomenda is NULL AND c.removido = 0');
        $select->bindValue(':id_cliente', $uid, PDO::PARAM_STR);
        $select->bindValue(':id_carrinho', $cid, PDO::PARAM_STR);
        $select->execute();
        return $select->fetch(PDO::FETCH_ASSOC);
      }
    }

    public function createCart($dados){
      // $cadastra = $this->mysql->prepare('INSERT INTO `carrinho`(`id_cliente`, `id_loja`, `id_cp`, `criado`) VALUES (:id_cliente, :id_loja, :id_cp, :criado);');
      $cadastra = $this->mysql->prepare('INSERT INTO `carrinho`(`id_cliente`, `criado`) VALUES (:id_cliente, :criado);');
      $cadastra->bindValue(':id_cliente', $dados['uid'], PDO::PARAM_STR);
      // $cadastra->bindValue(':id_loja', $dados['lid'], PDO::PARAM_INT);
      // $cadastra->bindValue(':id_cp', $dados['icp'], PDO::PARAM_INT);
      $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
      $cadastra->execute();
      $cart_id = $this->mysql->lastInsertId();
      // $hashi = array('cid'=>$cart_id, 'pid'=>1);
      // $this->createItemCart($hashi, null);
      $auto_insert_cart_itens = $this->ProdutosAction('auto_insert_cart');
      foreach ($auto_insert_cart_itens as $auto_insert_cart_item) {
        $item = array('cid'=>$cart_id, 'pid'=>$auto_insert_cart_item['id_produto'], 'min'=>$auto_insert_cart_item['min'], 'max'=>$auto_insert_cart_item['max']);
        $this->createItemCart($item, null);
      }
    }

    public function ProdutosAction($action){
      $consulta = $this->mysql->prepare("SELECT * FROM produtos_action WHERE `action` = :action AND ativo = 1");
      $consulta->bindValue(':action', $action, PDO::PARAM_STR);
      $consulta->execute();
      return $consulta->fetchAll(PDO::FETCH_ASSOC);
    }

    public function createItemCart($dados, $combo){
      $consulta = $this->mysql->prepare("SELECT * FROM carrinho_item WHERE `id_carrinho` = :id_carrinho AND `id_produto` = :id_produto AND removido = 0");
      $consulta->bindValue(':id_carrinho', $dados['cid'], PDO::PARAM_INT);
      $consulta->bindValue(':id_produto', $dados['pid'], PDO::PARAM_INT);
      $consulta->execute();
      $registro = $consulta->fetch(PDO::FETCH_ASSOC);

      $quantidade = empty($registro['quantidade']) ? 1 : $registro['quantidade'] + 1 ;

      if ($this->getProdQty($dados['pid']) < $quantidade) {
        return 'erro0';
      }

      if($registro && is_null($combo)){
        $quantidade = $registro['quantidade'] + 1 ;
        //$atualiza = $this->mysql->prepare('UPDATE `carrinho_item` SET `quantidade` = :quantidade, `atualizado` = :atualizado WHERE `id_carrinho` = :id_carrinho AND `id_produto` = :id_produto');
        $atualiza = $this->mysql->prepare('UPDATE `carrinho_item` SET `quantidade` = :quantidade, `atualizado` = :atualizado WHERE `id` = :id');
        //$atualiza->bindValue(':id_carrinho', $dados['cid'], PDO::PARAM_STR);
        //$atualiza->bindValue(':id_produto', $dados['pid'], PDO::PARAM_INT);
        $atualiza->bindValue(':quantidade', $quantidade, PDO::PARAM_INT);
        $atualiza->bindValue(':atualizado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $atualiza->bindValue(':id', $registro['id'], PDO::PARAM_INT);
        $atualiza->execute();
        $line_id = $registro['id'];
      } else {
        $cadastra = $this->mysql->prepare('INSERT INTO `carrinho_item`(`id_carrinho`, `id_produto`, `quantidade`, `min`, `max`, `criado`) VALUES (:id_carrinho, :id_produto, :quantidade, :min, :max, :criado);');
        $cadastra->bindValue(':id_carrinho', $dados['cid'], PDO::PARAM_STR);
        $cadastra->bindValue(':id_produto', $dados['pid'], PDO::PARAM_INT);
        $cadastra->bindValue(':quantidade', 1, PDO::PARAM_INT);
        $cadastra->bindValue(':min', $dados['min'], PDO::PARAM_INT);
        $cadastra->bindValue(':max', $dados['max'], PDO::PARAM_INT);
        $cadastra->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
        $cadastra->execute();
        $line_id = $this->mysql->lastInsertId();
      }
      if(!is_null($combo)){
        foreach ($combo as $key => $value) {
          $cadastra2 = $this->mysql->prepare('INSERT INTO `carrinho_group_item` (`id_linha`, `id_produto`, `quantidade`, `criado`) VALUES (:id_linha, :id_produto, :quantidade, :criado);');
          $cadastra2->bindValue(':id_linha', $line_id, PDO::PARAM_INT);
          $cadastra2->bindValue(':id_produto', $key, PDO::PARAM_INT);
          $cadastra2->bindValue(':quantidade', $value, PDO::PARAM_INT);
          $cadastra2->bindValue(':criado', date("Y-m-d H:i:s"), PDO::PARAM_STR);
          $cadastra2->execute();
        }
      }
    }

    //CRUD

    public function insertCat($dados){

        if($_SERVER['REQUEST_METHOD']=='POST'){
            $cadastra = $this->mysql->prepare('INSERT INTO produtos_cat (nome) VALUES (:nome);');
            $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
            $cadastra->execute();
        }
    }

    public function readCat($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE id = :id AND removido = 0');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch();
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE nome = :nome AND removido = 0');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll();
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos_cat WHERE 1 ORDER BY id ASC AND removido = 0;');
            $select->execute();
            return $select->fetchAll();
        }

        $select->execute();
        return $select->fetch();
    }

    public function editCat($dados){
        $deletef = $this->mysql->prepare('UPDATE produtos_cat SET nome = :nome WHERE id = :id ');
        $deletef->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $deletef->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $deletef->execute();
    }

    public function deleteCat($id){
        $deletef = $this->mysql->prepare('DELETE FROM produtos_cat WHERE id = :id;');
        $deletef->bindValue(':id', $id, PDO::PARAM_INT);
        $deletef->execute();
    }


}
