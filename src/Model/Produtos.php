<?php

class Produtos{

    public $mysql;

    public function __construct(Config $config){

        $this->mysql = $config->conn();

    }

    public function getLastProductsWithLimit($limit, $lg){
      $select = $this->mysql->prepare("SELECT p.id, p.referencia, e_nome.$lg as nome, e_desc.$lg as descricao, e_comp.$lg as composicao, image_url FROM `produtos` p
                                        INNER JOIN produtos_cat pc ON p.categoria = pc.id
                                        INNER JOIN exp e_nome ON p.nome = e_nome.id
                                        INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                        INNER JOIN exp e_comp ON p.composicao = e_comp.id
                                        WHERE p.grupo IN(1,2) AND p.ativo = 1 LIMIT $limit");
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getStoreProducts($lg, $limit=null){
      $limit = empty($limit) ? 1000 : $limit ;
      $select = $this->mysql->prepare("SELECT p.id, p.referencia, pc.slug as categoria, e_nome.$lg as nome, e_desc.$lg as descricao, e_comp.$lg as composicao, image_url,
                                        (SELECT json_extract(preco, '$.$lg') as preco FROM produtos WHERE referencia = p.referencia AND tipo = 0 AND ativo = 1 ORDER BY preco DESC LIMIT 1) as preco FROM `produtos` p
                                        INNER JOIN produtos_cat pc ON p.categoria = pc.id
                                        INNER JOIN exp e_nome ON p.nome = e_nome.id
                                        INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                        INNER JOIN exp e_comp ON p.composicao = e_comp.id
                                        WHERE p.tipo = 1 AND p.ativo = 1 AND p.grupo IN(0,2) ORDER BY id DESC LIMIT $limit");
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProducts($lg, $limit=null){
      $limit = empty($limit) ? 1000 : $limit ;
      $select = $this->mysql->prepare("SELECT p.id, p.referencia, pc.slug as categoria, json_extract(p.preco, '$.$lg') as preco, e_nome.$lg as nome, e_desc.$lg as descricao, e_comp.$lg as composicao, image_url FROM `produtos` p
                                        INNER JOIN produtos_cat pc ON p.categoria = pc.id
                                        INNER JOIN exp e_nome ON p.nome = e_nome.id
                                        INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                        INNER JOIN exp e_comp ON p.composicao = e_comp.id
                                        WHERE 1 ORDER BY id DESC LIMIT $limit");
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProductsStoreByCategorySlug($slug, $lg=null, $limit=null){
      $limit = empty($limit) ? 1000 : $limit ;
      if (empty($lg)) {
        $select = $this->mysql->prepare("SELECT * FROM `produtos` p INNER JOIN produtos_cat pc ON p.categoria = pc.id WHERE pc.slug = :categoria LIMIT $limit");
        $select->bindValue(':categoria', $slug, PDO::PARAM_STR);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      } else {
        $select = $this->mysql->prepare("SELECT p.id, p.referencia, e_nome.$lg as nome, e_desc.$lg as descricao, e_comp.$lg as composicao, image_url, 
        (SELECT json_extract(preco, '$.$lg') as preco FROM produtos WHERE referencia = p.referencia AND tipo = 0 AND ativo = 1 ORDER BY preco DESC LIMIT 1) as preco FROM `produtos` p
                                          INNER JOIN produtos_cat pc ON p.categoria = pc.id
                                          INNER JOIN exp e_nome ON p.nome = e_nome.id
                                          INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                          INNER JOIN exp e_comp ON p.composicao = e_comp.id
                                          WHERE p.tipo = 1 AND p.ativo = 1 AND p.grupo IN(0,2) AND pc.slug = :categoria LIMIT $limit");
        $select->bindValue(':categoria', $slug, PDO::PARAM_STR);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      }
    }

    public function getProductsByCategorySlug($slug, $lg=null){
      if (empty($lg)) {
        $select = $this->mysql->prepare("SELECT * FROM `produtos` p INNER JOIN produtos_cat pc ON p.categoria = pc.id WHERE pc.slug = :categoria");
        $select->bindValue(':categoria', $slug, PDO::PARAM_STR);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      } else {
        $select = $this->mysql->prepare("SELECT p.id, p.referencia, e_nome.$lg as nome, e_desc.$lg as descricao, e_comp.$lg as composicao, image_url FROM `produtos` p
                                          INNER JOIN produtos_cat pc ON p.categoria = pc.id
                                          INNER JOIN exp e_nome ON p.nome = e_nome.id
                                          INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                          INNER JOIN exp e_comp ON p.composicao = e_comp.id
                                          WHERE pc.slug = :categoria");
        $select->bindValue(':categoria', $slug, PDO::PARAM_STR);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      }
    }

    public function getCatalogByCategorySlug($slug, $lg=null){
      if (empty($lg)) {
        $select = $this->mysql->prepare("SELECT * FROM `produtos` p INNER JOIN produtos_cat pc ON p.categoria = pc.id WHERE pc.slug = :categoria");
        $select->bindValue(':categoria', $slug, PDO::PARAM_STR);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      } else {
        $select = $this->mysql->prepare("SELECT p.id, p.preco, p.referencia, e_nome.$lg as nome, e_desc.$lg as descricao, e_comp.$lg as composicao, image_url FROM `produtos` p
                                          INNER JOIN produtos_cat pc ON p.categoria = pc.id
                                          INNER JOIN exp e_nome ON p.nome = e_nome.id
                                          INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                          INNER JOIN exp e_comp ON p.composicao = e_comp.id
                                          WHERE p.grupo IN(1,2) AND p.ativo = 1 AND pc.slug = :categoria");
        $select->bindValue(':categoria', $slug, PDO::PARAM_STR);
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      }
    }

    public function getProductDetail($id, $lg){
      $select = $this->mysql->prepare("SELECT p.id, p.referencia, e_nome.$lg as nome, e_desc.$lg as descricao, e_comp.$lg as composicao, pc.slug as categoria, image_url, images_json,
                                        (SELECT json_extract(preco, '$.$lg') as preco FROM produtos WHERE referencia = p.referencia AND tipo = 0 AND ativo = 1 ORDER BY preco DESC LIMIT 1) as preco FROM `produtos` p
                                        INNER JOIN produtos_cat pc ON p.categoria = pc.id
                                        INNER JOIN exp e_nome ON p.nome = e_nome.id
                                        INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                        INNER JOIN exp e_comp ON p.composicao = e_comp.id
                                        WHERE p.id = :id");
      $select->bindValue(':id', $id, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getProductVariations($ref, $lg){
      $select = $this->mysql->prepare("SELECT pv.id_filho as id, p.referencia, json_extract(p.preco, '$.$lg') as preco, pav.valor as tamanho FROM `produtos` p
                                        INNER JOIN produtos_variacoes pv ON pv.id_filho = p.id
                                        INNER JOIN produtos_multivariacoes pm ON pv.id = pm.id_pai
                                        INNER JOIN produtos_atributos_valores pav ON pm.id_filho = pav.id
                                        WHERE p.referencia = :ref AND p.ativo = 1 GROUP BY p.id");
      $select->bindValue(':ref', $ref, PDO::PARAM_STR);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProductsByCategory($cid){
      $select = $this->mysql->prepare("SELECT * FROM `produtos` WHERE `categoria` = :categoria");
      $select->bindValue(':categoria', $cid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProductsByMarca($bid){
      $select = $this->mysql->prepare("SELECT * FROM `produtos` WHERE id_marca = :id_marca");
      $select->bindValue(':id_marca', $bid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProductsByMarcaAndCategory($bid, $cid){
      $select = $this->mysql->prepare("SELECT * FROM `produtos` WHERE `categoria` = :categoria AND id_marca = :id_marca");
      $select->bindValue(':categoria', $cid, PDO::PARAM_INT);
      $select->bindValue(':id_marca', $bid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function searchProducts($ocorrence, $lg){
      $select = $this->mysql->prepare("SELECT p.id, e_nome.$lg as nome, e_desc.$lg as descricao FROM produtos p
                                        INNER JOIN produtos_cat c ON p.categoria = c.id
                                        INNER JOIN exp e_nome ON p.nome = e_nome.id
                                        INNER JOIN exp e_desc ON p.descricao = e_desc.id
                                        WHERE p.ativo = 1 AND (p.referencia LIKE '%$ocorrence%' OR e_nome.$lg LIKE '%$ocorrence%' OR e_desc.$lg LIKE '%$ocorrence%'
                                        OR c.nome LIKE '%$ocorrence%')");
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function readProductsAndAllFamilies(){
      $select = $this->mysql->prepare('SELECT p.*, c.nome as categoria, m.nome as marca, a.nome as area FROM produtos p
                                        INNER JOIN produtos_cat c ON p.categoria = c.id
                                        INNER JOIN marcas m ON p.id_marca = m.id
                                        INNER JOIN areas a ON m.id_area = a.id
                                        WHERE p.ativo = 1
                                        ORDER BY m.id ASC');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function ativaProd($id){
        $delete = $this->mysql->prepare('UPDATE produtos SET ativo = 1 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }

    public function inativaProd($id){
        $delete = $this->mysql->prepare('UPDATE produtos SET ativo = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }

    public function getProdutoMarca($pid){
      $select = $this->mysql->prepare('SELECT p.*, m.nome as marca, m.logo as marca_logo, m.catalogo as catalogo, c.nome as categoria_nome FROM produtos p
                                        INNER JOIN marcas m ON p.id_marca = m.id
                                        INNER JOIN produtos_cat c ON p.categoria = c.id
                                        WHERE p.id = :id AND p.ativo = 1');
      $select->bindValue(':id', $pid, PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    public function getUserFiltredProducts($grupo){

      if($grupo == 0){
        $select = $this->mysql->prepare('SELECT p.*, c.nome as cat FROM produtos p
                                          INNER JOIN produtos_cat c ON p.categoria = c.id
                                          WHERE p.ativo = 1');
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      } else {
        $select = $this->mysql->prepare("SELECT p.*, c.nome as cat FROM produtos p
                                          INNER JOIN produtos_cat c ON p.categoria = c.id
                                          WHERE p.categoria IN(1,$grupo) AND p.ativo = 1");
        $select->execute();
        return $select->fetchAll(PDO::FETCH_ASSOC);
      }
    }

    public function getAllProdutoCategoria(){
      $select = $this->mysql->prepare('SELECT p.*, c.nome as cat FROM produtos p
                                        INNER JOIN produtos_cat c ON p.categoria = c.id
                                        WHERE p.ativo = 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProdutoCategoria(){
      $select = $this->mysql->prepare('SELECT p.*, c.nome as cat FROM produtos p
                                        INNER JOIN produtos_cat c ON p.categoria = c.id
                                        WHERE p.ativo = 1');
      $select->execute();
      return $select->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getProduto($pid){
      $select = $this->mysql->prepare('SELECT nome, image_url, preco FROM produtos WHERE id = :id AND ativo = 1 AND ativo = 1');
      $select->bindValue(':id', $pid  , PDO::PARAM_INT);
      $select->execute();
      return $select->fetch(PDO::FETCH_ASSOC);
    }

    //CRUD

    public function insertProd($dados){
      //$cadastra = $this->mysql->prepare('INSERT INTO produtos (nome, categoria, descricao, preco, ativo, image_url, id_marca, criado) VALUES (:nome, :categoria, :descricao, :preco, :ativo, :image_url, :id_marca, :criado);');
      $cadastra = $this->mysql->prepare('INSERT INTO produtos (referencia, nome, categoria, descricao, ativo, image_url, id_marca, criado) VALUES (:referencia, :nome, :categoria, :descricao, :ativo, :image_url, :id_marca, :criado);');
      $cadastra->bindValue(':referencia', $dados['referencia'], PDO::PARAM_STR);
      $cadastra->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
      $cadastra->bindValue(':categoria', $dados['categoria'], PDO::PARAM_STR);
      $cadastra->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
      //$cadastra->bindValue(':preco', $dados['preco'], PDO::PARAM_STR);
      $cadastra->bindValue(':ativo', 1, PDO::PARAM_INT);
      $cadastra->bindValue(':image_url', $dados['image_url'], PDO::PARAM_STR);
      $cadastra->bindValue(':id_marca', $dados['marca'], PDO::PARAM_INT);
      $cadastra->bindValue(':criado', date("Y-m-d"), PDO::PARAM_STR);
      $cadastra->execute();
    }

    public function readProd($id=null, $name=null){
      if(!empty($id)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos WHERE id = :id');
            $select->bindValue(':id', $id  , PDO::PARAM_INT);
            $select->execute();
            return $select->fetch(PDO::FETCH_ASSOC);
        } else if(!empty($name)) {
            $select = $this->mysql->prepare('SELECT * FROM produtos WHERE nome = :nome');
            $select->bindValue(':nome', $name  , PDO::PARAM_STR);
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }else {
            $select = $this->mysql->prepare('SELECT * FROM produtos WHERE 1 ORDER BY id ASC;');
            $select->execute();
            return $select->fetchAll(PDO::FETCH_ASSOC);
        }

        $select->execute();
        return $select->fetch();
    }

    public function editProd($dados){
      if(empty($dados['image_url'])){
        $update = $this->mysql->prepare('UPDATE produtos SET nome=:nome, categoria=:categoria, descricao=:descricao, preco=:preco, ativo=:ativo, atualizado=:atualizado WHERE id = :id;');
        $update->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $update->bindValue(':categoria', $dados['categoria'], PDO::PARAM_STR);
        $update->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
        $update->bindValue(':preco', $dados['preco'], PDO::PARAM_STR);
        $update->bindValue(':ativo', 1, PDO::PARAM_INT);
        $update->bindValue(':atualizado', date("Y-m-d"), PDO::PARAM_STR);
        $update->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $update->execute();
      } else {
        $update = $this->mysql->prepare('UPDATE produtos SET nome=:nome, categoria=:categoria, descricao=:descricao, preco=:preco, ativo=:ativo, image_url=:image_url, atualizado=:atualizado WHERE id = :id;');
        $update->bindValue(':nome', $dados['nome'], PDO::PARAM_STR);
        $update->bindValue(':categoria', $dados['categoria'], PDO::PARAM_STR);
        $update->bindValue(':descricao', $dados['descricao'], PDO::PARAM_STR);
        $update->bindValue(':preco', $dados['preco'], PDO::PARAM_STR);
        $update->bindValue(':ativo', 1, PDO::PARAM_INT);
        $update->bindValue(':image_url', $dados['image_url'], PDO::PARAM_STR);
        $update->bindValue(':atualizado', date("Y-m-d"), PDO::PARAM_STR);
        $update->bindValue(':id', $dados['id'], PDO::PARAM_INT);
        $update->execute();
      }
    }

    public function deleteProd($id){
        $delete = $this->mysql->prepare('UPDATE produtos SET ativo = 0 WHERE id = :id;');
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }


}
