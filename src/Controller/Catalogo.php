<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$loader->get('src/Model/Produtos');
$prod = new Produtos(new Config());
$loader->get('src/Model/Categorias');
$cat = new Categorias(new Config());
$loader->get('src/Model/Marcas');
$marcas = new Marcas(new Config());
$loader->get('src/Model/Mail');

$mensagem_erro = false;
$module_data['mod_title'] = $config->getExpById(1, $_SESSION['lg'])['lg'];

if (Request::isAjax()){

  Resources::ajaxProtect($_POST, $_SESSION);

  if($action == 'ativar'){
    $prod->ativaProd($_POST['id']);
  } else if($action == 'inativar'){
    $prod->inativaProd($_POST['id']);
  }

  if($action == 'send'){

    $produto = $prod->getProdutoMarca(array_pop($_POST['dados'])['value']);

    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
    $html = '<h1>Pedido de Orçamento</h1>';
    $html .= '<p>O cliente '.$_POST['dados'][0]['value'].' pediu um orçamento.</p>';
    $html .= '<p>Foi enviado o email de contacto '.$_POST['dados'][1]['value'].' com a seguinte mensagem:</p>';
    $html .= '<p>'.$_POST['dados'][2]['value'].'</p>';
    // $html .= '<p>Qualquer dúvida entre em contacto.</p>';

    // if ($_POST['dados'][2]['value'] !== 'on') {
    //   echo json_encode(array('0', '<strong>Erro!</strong> Deve aceitar a nossa política de privacidade.'));
    //   exit();
    // }

    if (SendMail::trySendMail('Pedido de orçamento Iora Lingerie', 'cristina@ioralingerie.com', $html, new PHPMailer(true))) {
      echo json_encode(array('1', '<strong>Sucesso!</strong> Pedido de orçamento enviado com sucesso.'));
      exit();
    } else {
      echo json_encode(array('0', '<strong>Erro!</strong> O sistema não conseguiu enviar o e-mail.'));
      exit();
    }
    //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------


  }

}

if($action == 'index'){

  $produtos = $prod->getCatalogByCategorySlug($action, $_SESSION['lg']);
  $module_data['produtos'] = $produtos;
  $module_data['actual_category'] = '';

  // echo '<pre>';
  // print_r($action);
  // echo '</pre>';
  // exit();

} else if($action == 'detalhe'){

  // $produto = $prod->getProductDetail($param, $_SESSION['lg']);
  // $module_data['produto'] = $produto;

  // echo '<pre>';
  // print_r($produto);
  // echo '</pre>';
  // exit();

} else {

  $produtos = $prod->getCatalogByCategorySlug($action, $_SESSION['lg']);
  $module_data['produtos'] = $produtos;
  $module_data['actual_category'] = '';

  // echo '<pre>';
  // print_r($produtos);
  // echo '</pre>';
  // exit();

  // if (empty($param)) {
  //   $categoria = $cat->getCategoryBySlug($action);
  //   $produtos = $prod->getProductsByCategory($categoria['id']); //se for so action e so categoria
  // } else {
  //   $produtos = $prod->getProductsByMarcaAndCategory($action, $param); //se for action e param acction e marca e para categoria
  // }
  //
  // $module_data['produtos'] = $produtos;
  // $module_data['actual_category'] = $categoria['nome'];
}

  // echo '<pre>';
  // print_r($config->getMegaMenu($_SESSION['lg']));
  // echo '</pre>';

?>
