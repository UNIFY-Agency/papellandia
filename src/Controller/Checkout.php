<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

$checkout = new Checkout(new Config());
$loader->get('src/Model/Lojas');
$lojas = new Lojas(new Config());
$loader->get('src/Model/Portes');
$portes = new Portes(new Config());
$loader->get('src/Model/Cupom');
$cupom = new Cupom(new Config());
$loader->get('src/Model/Mail');

$module_data['hide_slide'] = 1;

// if(SendMail::trySendMail('teste testão', 'renan.pantoja@hotmail.com', '<table>aqui estão</table>', new PHPMailer(true))){
//   echo 'enviou';
// } else {
//   echo 'não enviou';
// }

// if(empty($_SESSION['entrega']['cp']) || empty($_SESSION['carrinho'])){ header('Location: '.URL_BASE); }
if(empty($_SESSION['carrinho']['itens'])){ header('Location: '.URL_BASE); }
if($_SESSION['carrinho']['total']['total'] < 15){ header('Location: '.URL_BASE.'/Cart'); }

$loja_ver = $lojas->LojaMarcaById($_SESSION['carrinho']['id_loja']);

$mensagem_erro = false;

// if(isset($_SESSION['pre-order'])){
//   $pre_order_msg = 'Está no modo de adiantamento de pedido, coloque os seus produtos no carrinho normalmente, o seu pedido será entregue às '.$_SESSION['pre-order']['hora'];
// }

//------------------------------------------------------------------------CALCULO DE VALOR FINAL-----------------------------------------------------------

if(!empty($_SESSION['cupom']) && $_SESSION['cupom']['tipo'] != 3){
  if($_SESSION['cupom']['tipo'] == 1){
    $total_do_checkout = number_format($_SESSION['carrinho']['total']['total'] - ($_SESSION['carrinho']['total']['total'] * ($_SESSION['cupom']['valor'] / 100)), 2);
  } else if($_SESSION['cupom']['tipo'] == 2){
    $total_do_checkout = number_format($_SESSION['carrinho']['total']['total'] - $_SESSION['cupom']['valor'], 2);
  }
} else {
  $total_do_checkout = number_format($_SESSION['carrinho']['total']['total'], 2);
}

//------------------------------------------------------------------------CALCULO DE VALOR FINAL-----------------------------------------------------------

$payments = $checkout->getPaymentMethods();
$module_data['payments'] = $payments;
$paises = $checkout->getAllowCountries();
$module_data['paises'] = $paises;
$module_data['total'] = $total_do_checkout;
$module_data['send_methods'] = $portes->getSendMethods();

// echo '<pre>';
// print_r($_POST);
// echo '</pre>';

if(Request::isAjax()){

  Resources::ajaxProtect($_POST, $_SESSION);

  if($action == 'getTotalCheckout'){
    echo json_encode(array('1', $total_do_checkout));
    exit();
  }

  if($action == 'getPortRule'){
    if($_POST['dados'][0]['value']){
      $rules = $portes->getPorteRules($_POST['dados'][0]['value']);
      echo json_encode(array('1', json_encode($rules)));
    } else {
      echo json_encode(array('0', '<strong>Erro!</strong> Dados em falta.'));
    }
  }

  if($action == 'get-cupom'){

    if($_SESSION['carrinho']['total']['total'] < 15){
      echo json_encode(array('0', '<strong>Erro!</strong> Cupões não podem ser usados em compras inferiores a 15€.'));
      exit();
    }

    // echo '<pre>';
    // print_r($cupom->getCupom($_POST['dados'][0]['value'], $_SESSION));
    // echo '</pre>';
    // die();

    $clientcupom = $cupom->getCupom($_POST['dados'][0]['value'], $_SESSION);
    if(substr($clientcupom, 0, 4) == 'erro'){
      $erro = substr($clientcupom, 4);
      switch (true) {
        case $erro == 1:
          echo json_encode(array('0', '<strong>Erro!</strong> Não existe um cupão com o código enviado.'));
          exit();
          break;
        case $erro == 2:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse cupão não está habilitado para esta loja.'));
          exit();
          break;
        case $erro == 3:
          echo json_encode(array('0', '<strong>Erro!</strong> Para utilizar este cupão é necessário ter uma conta, crie uma conta ou faça login.'));
          exit();
          break;
        case $erro == 4:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse cupão já atingiu o limite de uso.'));
          exit();
          break;
        case $erro == 5:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse cupão não pode ser usado mais de uma vez.'));
          exit();
          break;
        case $erro == 6:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse cupão aplica-se apenas à primeira compra.'));
          exit();
          break;
      }
    } else {
      if($clientcupom['tipo'] == 3){
        $dados['pid'] = $clientcupom['valor'];
        $dados['cid'] = $_SESSION['carrinho']['id'];
        $dados['min'] = 1;
        $dados['max'] = 1;
        $carrinhos->createItemCart($dados, NULL, 0);
      }
      $_SESSION['cupom'] = $clientcupom;
      echo json_encode(array('1', $_POST['dados'][0]['value']));
      exit();
    }
  }

  if($action == 'remove-cupom'){

    if($_SESSION['cupom']['tipo'] == 3){
      $carrinhos->removeCartCupomItem($_SESSION['carrinho']['id'], $_SESSION['cupom']['valor']);
    }

    $cupom->saveCupom($_SESSION['cupom']['id']);
    unset($_SESSION['cupom']);
    echo json_encode(array('1', 'cupões apagados'));
    exit();
  }

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

  if($action == 'makeOrder'){

    $dados = array();
    foreach ($_POST['dados'] as $key => $value) {
      $dados[$value['name']] = $value['value'];
    }

    $_POST = $dados;

    if ($_POST['pago']) {
      $_SESSION['online']['pago'] = $_POST['pago'];
    }

    // echo '<pre>';
    // print_r($dados);
    // echo '</pre>';
    // die();

    $_POST = array_map("strip_tags", $_POST);

    switch (true) {
      case empty($_POST['nome']):
      $mensagem_erro = 'O nome ficou em falta!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      case empty($_POST['email']):
      $mensagem_erro = 'O e-mail ficou em falta!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      case empty($_POST['tel2']) || $_POST['tel2'] == 0:
      $mensagem_erro = 'O contacto ficou em falta!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

  	  case strlen($_POST['tel2']) < 9:
      $mensagem_erro = 'O contacto deve ter 9 dígitos ou mais!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      case empty($_POST['localidade']):
      $mensagem_erro = 'A localidade ficou em falta!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      case empty($_POST['morada']):
      $mensagem_erro = 'O nome da rua ficou em falta!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      // case empty($_POST['numero']):
      // $mensagem_erro = 'O número da rua ficou em falta!';
      // echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      // exit();
      // break;

      case empty($_POST['payment-option']) || $_POST['payment-option'] == 0:
      $mensagem_erro = 'Selecione uma forma de pagamento!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      // case empty($_POST['entrega']) || $_POST['entrega'] == 0:
      // $mensagem_erro = 'Selecione uma forma de entrega!';
      // echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      // exit();
      // break;

      case empty($_POST['termos']) && !$_SESSION['usuario']['logged']:
      $mensagem_erro = 'Tem que concordar com a política de privacidade e termos de uso.';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      case empty($_POST['password']) && !$_SESSION['usuario']['logged']:
      $mensagem_erro = 'A senha ficou em falta!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      case empty($_POST['re-password']) && !$_SESSION['usuario']['logged']:
      $mensagem_erro = 'Precisa de digitar a senha novamente!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      case $_POST['password'] !== $_POST['re-password']  && !$_SESSION['usuario']['logged']:
      $mensagem_erro = 'As senhas são diferentes!';
      echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
      exit();
      break;

      // case $_POST['password'] !== $_POST['re-password']:
      // $mensagem_erro = 'As senhas são diferentes!';
      // break;

      case (!empty($_POST['nome']) and !empty($_POST['email']) and !empty($_POST['tel2']) and !empty($_POST['morada']) and !empty($_POST['localidade'])):

        $falha = false;

        // if(!$users->validateNIF((int)$_POST['nif']) && (int)$_POST['nif'] !== 0){
        //   $mensagem_erro = 'O NIF inserido não é válido!';
        //   echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
        //   exit();
        //   $falha = true;
        // }

        // if(!$lojas->lojaAberta($_SESSION['carrinho']['id_loja']) && empty($_SESSION['pre-order'])){
        //   $mensagem_erro = 'A loja em que está a tentar fazer o pedido encontra-se fechada.';
        //   echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
        //   exit();
        //   $falha = true;
        // }
        //
        // $codigo_postal = $lojas->getEndereco($_SESSION['entrega']['cp']);
        // if($codigo_postal['id_loja'] !== $_SESSION['carrinho']['id_loja']){
        //   $mensagem_erro = 'O seu código postal não coincide com a loja em que o carrinho foi criado, esvazie o seu carrinho e tente novamente.';
        //   echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
        //   exit();
        //   $falha = true;
        // }

        if(empty($carrinhos->getCart($_SESSION['usuario']['id']))) {
          $mensagem_erro = 'Esse carrinho já foi utilizado noutra encomenda, por favor crie outro carrinho e tente novamente.';
          echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
          exit();
          $falha = true;
        }

        //o vetor news nao pode ir vazio se não causa um erro no userBasicRegister
        if(empty($_POST['news'])){ $_POST['news'] = 0; } else { $_POST['news'] = 1; }

        if($_SESSION['usuario']['logged']){
          if((int)$_POST['nif'] !== 0){
              $users->cadastraNIF($_SESSION['usuario']['id'], (int)$_POST['nif']);
          }
          $usuario = $_SESSION['usuario'];
        } else {
          if(empty($_POST['password']) || empty($_POST['re-password']) || empty($_POST['re-password']) && empty($_POST['password'])){
            $mensagem_erro = 'A senha ficou em falta!';
            echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
            exit();
            $falha = true;
          } else {
            $cadastrar = $users->userBasicRegister($_POST);
            if(substr($cadastrar, 0, 4) == 'erro'){
              $falha = true;
              $erro = substr($cadastrar, 4);
              switch (true) {
                case $erro == 1:
                  $usuario = $users->retUsuario(strip_tags($_POST['email']));
                  //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                  $html = '<h1>Link de redefinição de senha para a Iora Lingerie</h1>';
                  $link_password = URL_BASE.'/user/redefinir_senha/'.md5($usuario['id']).md5(date('YmdH'));
                  $html .= '<a href="'.$link_password.'">Clique redefinir a sua senha</a>';
                  $html .= '<p>Atenção, este link tem a validade de apenas uma hora.</p>';

                  //email de recuperação de senha
                  SendMail::trySendMail('Recuperação de senha', $usuario['email'], $html, new PHPMailer(true));
                  // echo '<pre>';
                  // print_r($usuario);
                  // echo '</pre>';
                  $mensagem_erro = 'O seu e-mail está registado mas não há senha, foi enviado um link para o seu e-mail para redefinir a senha (procure também na pasta de spam).';
                  echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
                  exit();
                  break;
                case $erro == 2:
                  $mensagem_erro = 'Esse e-mail já está registado no nosso sistema.';
                  echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
                  exit();
                  break;
              }
            } else {
              $usuario = $users->readUsuario(null, $cadastrar);
              $users->force_login($usuario);
            }
          }
        }

        // if(!$falha){
        //   echo 'passou em tudo';
        // } else {
        //   echo 'algo deu errado';
        // }
        // if(false){

        if(!$falha){

          // echo $mensagem_erro;
          // echo '<pre>';
          // print_r($usuario);
          // echo '</pre>';
          // echo '<pre>';
          // print_r($_POST);
          // echo '</pre>';
          // exit();

          // $loja_checkout = $lojas->getLojaMarca($_SESSION['carrinho']['id_loja']);
          $loja_ver['entrega_gratis'] = true;

          //taxa de entrega e valor final
          if($loja_ver['entrega_gratis']){
            $total_final = number_format($_SESSION['carrinho']['total']['total'],2);
            $taxa_entrega_final = 0.00;
          } else {
            if($_POST['entrega'] == 2){
              $total_final = number_format($_SESSION['carrinho']['total']['total'],2);
              $taxa_entrega_final = 0.00;
            } else {
              $total_final = $_SESSION['entrega']['taxa'] == 0.00 ? number_format($_SESSION['carrinho']['total']['total'],2) : number_format(($_SESSION['carrinho']['total']['total'] + $_SESSION['entrega']['taxa']), 2);
              $taxa_entrega_final = number_format($_SESSION['entrega']['taxa'], 2);
            }
          }

          $_POST['entrega'] = 1;
          //tipo de entrega
          if(isset($_SESSION['pre-order'])){
            if($_POST['entrega'] == 1){
              $tipo_entrega = 3;
            } else {
              $tipo_entrega = 4;
            }
          } else {
            $tipo_entrega = $_POST['entrega'];
          }

          //cupom
          // if(!empty($_SESSION['cupom']) && $_SESSION['cupom']['tipo'] != 3){
          //   if($_SESSION['cupom']['tipo'] == 1){
          //     $total_final = number_format($total_final - ($total_final * ($_SESSION['cupom']['valor'] / 100)), 2);
          //   } else if($_SESSION['cupom']['tipo'] == 2){
          //     $total_final = number_format($total_final - $_SESSION['cupom']['valor'], 2);
          //   }
          // } else {
          //   $total_final = $total_final;
          // }


          $dados['uid'] = $usuario['id'];
          $dados['cid'] = $_SESSION['carrinho']['id'];
          $dados['lid'] = $_SESSION['carrinho']['id_loja'];
          $dados['total'] = $total_final;
          $dados['pagamento'] = $_POST['payment-option'];
          $dados['pago'] = empty($_SESSION['online']['pago']) ? NULL : $_SESSION['online']['pago'] ;
          $dados['troco'] = 0;
          $dados['entrega'] = $tipo_entrega;
          $dados['taxa_entrega'] = $taxa_entrega_final;
          $dados['adiantado'] = empty($_SESSION['pre-order']) ? NULL : $_SESSION['pre-order']['hora'] ;
          $dados['fatura'] = $_POST['nif'] == 0 ? 0 : 1 ;
          $dados['status'] = empty($_SESSION['online']['pago']) ? 1 : 2 ;
          $dados['pontos'] = (10 / 100) * $_SESSION['carrinho']['total']['total'];
          $dados['freguesia'] = strip_tags($_POST['localidade']);
          $dados['rua'] = strip_tags($_POST['morada']);
          $dados['numero'] = strip_tags(0);
          $dados['complemento'] = strip_tags($_POST['distrito']);
          $dados['ref'] = $checkout->buildOrderReference($loja_checkout);

          $id_encomenda = $checkout->createOrder($dados);
          $encomenda = $checkout->getOrderById($id_encomenda);

          // if(!empty($_SESSION['cupom']) && $_SESSION['cupom']['tipo'] != 3){ $checkout->linkCupomToOrder($id_encomenda, $_SESSION['cupom']['id']); unset($_SESSION['cupom']); }

          unset($_SESSION['carrinho']);

          //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
          $html = '<h1>Um novo pedido para a sua loja foi feito no sistema Iora Lingerie.</h1>';
          $html .= '<p>O pedido novo tem a referência '.$dados['ref'].'</p>';
          $html .= '<p>Faça login na sua conta Admin para ver todos os seus pedidos.</p>';
          $html .= '<a href="'.URL_BASE.'/bo'.'">Clique aqui para aceder</a>';

          //email avisando o franqueado sobre o pedido
          SendMail::trySendMail('Novo pedido', 'cristina@ioralingerie.com', $html, new PHPMailer(true));

          $espera = explode('.', $loja_checkout['espera']);
          $html = '<h1>Recebemos o seu pedido, código referência '.$dados['ref'].'</h1>';
          $html .= '<p>Veja os detalhes da sua encomenda no seu perfil de utilizador <a href="'.URL_BASE.'/perfil">clicando aqui</a></p>';
          // $html .= '<p>O tempo de espera será de aproximadamente '.$espera[0].' a '.$espera[1].' minutos.</p>';
          // $html .= '<p>Qualquer dúvida entre em contacto com o número '.$loja_checkout['tel'].'.</p>';
          if (empty($_SESSION['online']['pago'])) { //sem pagamento online
            $bank = $checkout->getPaymentMethodsById(1);
            $html .= '<p>'.$bank['instructions'].'</p>';
            $html .= '<p>'.implode('<br>', json_decode($bank['instructions'], true)).'</p>';
            $html .= '<p>Obrigado pela sua encomenda. Está a aguardar até que confirmemos que o pagamento foi recebido.</p>';
          } else {
            $html .= '<p>Recebemos o pagamento da sua encomenda com sucesso. Irá agora ser tratada:</p>';
          }
          $html .= '<p>Esperamos satisfazer a sua encomenda em breve.</p>';

          //email o usuário que fez o pedido
          SendMail::trySendMail('Recebemos o seu pedido!', $_SESSION['usuario']['email'], $html, new PHPMailer(true));

          //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------

          // echo '<pre>';
          // print_r($dados);
          // echo '</pre>';
          // exit();

          if(isset($_SESSION['pre-order'])){
            unset($_SESSION['pre-order']);
          }

          $_SESSION['encomenda'] = $encomenda;
          // header('Location: '.URL_BASE.'/loja/concluido');
          echo json_encode(array('1', json_encode('')));
          exit();
        }

      break;

      default:
        $mensagem_erro = 'Switch Default.';
        echo json_encode(array('0', '<strong>Erro!</strong> '.$mensagem_erro));
        exit();
      break;
    }
  }
}

?>
