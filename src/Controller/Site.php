<?php

$loader->get('src/Model/Produtos');
$prod = new Produtos(new Config());

$produtos = $prod->getLastProductsWithLimit(4, $_SESSION['lg']);
$module_data['produtos'] = $produtos;

if (Request::isAjax() && $action == 'location'){
    $_SESSION['location'] = geoip_country_code_by_name($_SERVER['REMOTE_ADDR']); //geoip_country_code_by_name need a PECL extension GEOIP
    $_SESSION['lg'] = $_SESSION['location'] == 'PT' ? strtolower($_SESSION['location']) : 'en' ;
    // echo 'local '.$_SESSION['location'].' | lang '.$_SESSION['lg'];
    exit();
}

//expressoes para header menu e footer
//$module_data = $expressions->getExp(array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28), $_SESSION['lg']);

// echo $lg;
//
// echo '<pre>';
// print_r($produtos);
// echo '</pre>';

?>
