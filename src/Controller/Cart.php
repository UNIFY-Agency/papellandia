<?php

$loader->get('src/Model/Produtos');
$produtos = new Produtos(new Config());
$loader->get('src/Model/Lojas');
$lojas = new Lojas(new Config());

$module_data['hide_slide'] = 1;

// $loja_aberta = $lojas->lojaAberta($_SESSION['entrega']['loja']);
// $loja_ver = $lojas->LojaMarcaById($_SESSION['entrega']['loja']);

if (!Request::isAjax()){
  // if(empty($_SESSION['entrega']['cp']) || empty($_SESSION['carrinho'])){ header('Location: '.URL_BASE); }
  // if(empty($_SESSION['carrinho']['itens'])){ header('Location: '.URL_BASE.'/produtos'); }

  if(isset($_SESSION['pre-order'])){
    $pre_order_msg = 'Está no modo de adiantamento de pedido, coloque os seus produtos no carrinho normalmente, o seu pedido será entregue às '.$_SESSION['pre-order']['hora'];
  }

//------------------------------------------------------------------------CALCULO DE VALOR FINAL-----------------------------------------------------------

  if(!empty($_SESSION['cupom']) && $_SESSION['cupom']['tipo'] != 3){
    if($_SESSION['cupom']['tipo'] == 1){
      $total_do_carrinho = number_format($_SESSION['carrinho']['total']['total'] - ($_SESSION['carrinho']['total']['total'] * ($_SESSION['cupom']['valor'] / 100)), 2);
    } else if($_SESSION['cupom']['tipo'] == 2){
      $total_do_carrinho = number_format($_SESSION['carrinho']['total']['total'] - $_SESSION['cupom']['valor'], 2);
    }
  } else {
    $total_do_carrinho = number_format($_SESSION['carrinho']['total']['total'], 2);
  }

//------------------------------------------------------------------------CALCULO DE VALOR FINAL-----------------------------------------------------------

  // echo '<pre>';
  // print_r($loja_ver);
  // echo '</pre>';

}

if($_SESSION['usuario']['id'] == 1){

}

if (Request::isAjax()){

  //proteção contra pedidos ajax ---------------------------------------------------------------------------

  //verifico se faz mais de 3 segundos da última chamada para evitar ataques
  if(isset($_SESSION['usuario']['lastajaxcall']) && (strtotime('now') - $_SESSION['usuario']['lastajaxcall']) < 1){
      echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Chamada com menos de 1 segundo! - Agora: '.date('s', strtotime('now')).', Última: '.date('s',$_SESSION['usuario']['lastajaxcall'])));
      exit();
  }
  $_SESSION['usuario']['lastajaxcall'] = strtotime('now');

  //verifico se o id de uauario que foi passado na chamada e o mesmo que o de sessão
  $session_id = array_pop($_POST['dados']);
  if($session_id['value'] !== $_SESSION['usuario']['id']){
      echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Alerta de tentativa de violação de acesso, tentativa de invasão registada.'));
      exit();
  }

  //proteção contra pedidos ajax ---------------------------------------------------------------------------

  if($action == 'set-pre-order'){
    switch (true) {
      case $loja_aberta:
        echo json_encode(array('0', '<strong>Erro!</strong> Impossível adiantar, a loja está aberta.'));
        exit();
        break;
      case !strtotime($_POST['dados'][0]['value']):
        echo json_encode(array('0', '<strong>Erro!</strong> O formato da hora é inválido!'));
        exit();
        break;
      case strtotime($_POST['dados'][0]['value']):
        $_SESSION['pre-order']['hora'] = $_POST['dados'][0]['value'];
        echo json_encode(array('1', '<i class="fas fa-check"></i><strong>Sucesso!</strong> A redirecionar para adiantamento. <span id="countdown">4</span>.', URL_BASE.'/lojas/ver/'.$param));
        exit();
        break;
    }
  }

  if($action == 'cancel-pre-order'){
    if($_SESSION['carrinho']){
      $carrinhos->purgeCart($_SESSION['carrinho']['id']);
      unset($_SESSION['carrinho']);
    }
    unset($_SESSION['pre-order']);
  }

  if($action == 'set-alergias'){
    if(empty($_POST['dados'])){
      $carrinhos->setCartAllergyes($_SESSION['carrinho']['id'], NULL);
    } else {
      $carrinhos->setCartAllergyes($_SESSION['carrinho']['id'], implode(',', array_column($_POST['dados'], 'value')));
    }
  }

  if($action == 'plus' || $action == 'minus'){
    $alteration = $carrinhos->AlterCartLineQty($action, preg_replace("/[^0-9]/", "", $_POST['dados'][0]['value']));
    if(substr($alteration, 0, 4) == 'erro'){
      $erro = substr($alteration, 4);
      switch (true) {
        case $erro == 1:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse item não pode conter uma quantidade maior que a atual.'));
          exit();
          break;
        case $erro == 2:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse item não pode conter uma quantidade menor que a atual.'));
          exit();
          break;
      }
    } else {
      echo json_encode(array('1', 'true'));
      exit();
    }
  }

  if($action == 'qty'){
    // $alteration = $carrinhos->AlterCartLineQty($action, preg_replace("/[^0-9]/", "", $_POST['dados'][0]['value']));
    $alteration = $carrinhos->AlterCartLineQty($_POST['dados'][1]['value'], $_POST['dados'][0]['value']);
    if(substr($alteration, 0, 4) == 'erro'){
      $erro = substr($alteration, 4);
      switch (true) {
        case $erro == 0:
          echo json_encode(array('0', '<strong>Erro!</strong> Não há quantidade em stock maior do que a atual para esse item!'));
          exit();
          break;
        case $erro == 1:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse item não pode conter uma quantidade maior que a atual.'));
          exit();
          break;
        case $erro == 2:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse item não pode conter uma quantidade menor que a atual.'));
          exit();
          break;
      }
    } else {
      echo json_encode(array('1', 'true'));
      exit();
    }
  }

  if($action == 'freestyle-options'){
    if($_POST['dados'][0]['value']){
      $freestyle_options = $produtos->getFreestyleOptions($_POST['dados'][0]['value']);
      echo json_encode(array('1', json_encode($freestyle_options)));
    } else {
      echo json_encode(array('0', '<strong>Erro!</strong> Falta de dados.'));
    }
  }

  if($action == 'get_combo'){

    if(!$loja_aberta && empty($_SESSION['pre-order'])){
        echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Não pode adicionar itens dessa loja ao carrinho, pois a mesma encontra-se fechada.'));
        exit();
    }

    if(empty($_SESSION['entrega']['cp'])){
        echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Lamentamos, nenhuma loja entrega neste endereço.'));
        exit();
    }

    if($_SESSION['entrega']['loja'] != $_POST['dados'][1]['value']){
        echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Está a adicionar itens de uma loja que atende outra localidade, se pretende pedir nessa loja altere o seu código postal no início do site.'));
        exit();
    }

    if(!empty($_SESSION['carrinho']['id_loja']) && $_SESSION['carrinho']['id_loja'] != $_POST['dados'][1]['value']){
        echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Não pode misturar produtos de lojas diferentes no mesmo carrinho. Esvazie o seu carrinho e tente novamente.'));
        exit();
    }

    $combo = $produtos->getLojaCombo($_POST['dados'][0]['value'], $_POST['dados'][1]['value']);
    $cat_max = array_keys(array_column($combo, 'cat_max', 'categoria'));
    $combo_quant = array_sum(array_map('intval', array_column($combo, 'cat_max', 'categoria')));
    // echo '<pre>';
    // print_r($combo);
    // echo '</pre>';
    //echo json_encode($colunas);
    echo json_encode(array('1', json_encode($combo), json_encode($cat_max), $combo_quant));
    exit();
  }

  if($action == 'add'){

    // if(!$loja_aberta && empty($_SESSION['pre-order'])){
    //     echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Não pode adicionar itens dessa loja ao carrinho, pois a mesma encontra-se fechada.'));
    //     exit();
    // }

    // if(empty($_SESSION['entrega']['cp'])){
    //     echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Lamentamos, nenhuma loja entrega neste endereço.'));
    //     exit();
    // }

    if($_POST['combo'] == 1){

      $max = array_pop($_POST['dados'])['value'];
      $min = array_pop($_POST['dados'])['value'];
    	$lid = array_pop($_POST['dados'])['value'];
    	$pid = array_pop($_POST['dados'])['value'];
      $freestyle = array_pop($_POST['dados'])['value'];
      $combo = $produtos->getLojaCombo($pid, $lid);
      $cat_max = array_column($combo, 'cat_max', 'categoria');
      $combo_quant = array_sum(array_map('intval', array_column($combo, 'cat_max', 'categoria')));
      $combo_prod = array_column($combo, 'id_produto');

    	foreach ($_POST['dados'] as $key => $value) {
    		if($value['value'] != 0){
    			$chave = str_replace('prod-', '', $value['name']);
    			$combo_itens[$chave] = $value['value'];
          $chave_combo = array_search($chave, $combo_prod);
          $cat_quant[$combo[$chave_combo]['categoria']] = $cat_quant[$combo[$chave_combo]['categoria']] + $value['value'];

          if(!$freestyle){
            switch (true) {
              case $value['value'] < $combo[$chave_combo]['min']:
                echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Deve-se respeitar a capacidade mínima de cada item.'));
                exit();
                break;
              case $value['value'] > $combo[$chave_combo]['max']:
                echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Foi extrapolada a capacidade máxima de algum item.'));
                exit();
                break;
            }
          }
    		}
      }

      if(!$freestyle){
        if(array_sum($combo_itens) != $combo_quant){
          echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> A quantidade de itens deve ser igual a '.$combo_quant.'.'));
          exit();
        }
        if(!empty(array_diff_assoc($cat_max, $cat_quant))){
          echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Extrapolada a capacidade máxima de categoria.'));
          exit();
        }
      }
    } else {
    	// $lid = $_POST['dados'][1]['value'];
    	$pid = $_POST['dados'][0]['value'];
      $min = empty($_POST['dados'][2]['value']) ? null : null ;
      $max = empty($_POST['dados'][3]['value']) ? null : null ;
      $combo_itens = null;
    }

    // if($_SESSION['entrega']['loja'] != $lid){
    //     echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Está a adicionar itens de uma loja que atende outra localidade, se pretende pedir nessa loja altere o seu código postal no início do site.'));
    //     exit();
    // }
    //
    // if(!empty($_SESSION['carrinho']['id_loja']) && $_SESSION['carrinho']['id_loja'] != $lid){
    //     echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Não pode misturar produtos de lojas diferentes no mesmo carrinho. Esvazie o seu carrinho e tente novamente.'));
    //     exit();
    // }

    // echo '<pre>';
    // print_r($cat_max);
    // echo '</pre>';
    // exit();

    // $produto = $produtos->getLojaProduto($pid, $lid);
    $produto = $produtos->getProduto($pid);
    if($produto){
      $dados['pid'] = $pid;
      $dados['lid'] = $lid;
      $dados['min'] = $min;
      $dados['max'] = $max;
      $dados['uid'] = $_SESSION['usuario']['id'];
      // $dados['icp'] = $lojas->getEndereco($_SESSION['entrega']['cp'])['id'];

      $carrinho = $carrinhos->getCart($_SESSION['usuario']['id']);
      if(empty($carrinho)){
        $carrinhos->createCart($dados);
        $carrinho = $carrinhos->getCart($_SESSION['usuario']['id']);
        $_SESSION['carrinho'] = $carrinho;
        $dados['cid'] = $carrinho['id'];
        $createitemcart = $carrinhos->createItemCart($dados, $combo_itens);
        if(substr($createitemcart, 0, 4) == 'erro'){
          $erro = substr($alteration, 4);
          switch (true) {
            case $erro == 0:
              echo json_encode(array('0', '<strong>Erro!</strong> Não há quantidade suficiente em stock para esse item!'));
              exit();
              break;
          }
        } else {
          $_SESSION['carrinho']['itens'] = $carrinhos->getCartItens($_SESSION['usuario']['id'], $carrinho['id']);
          $total = $carrinhos->getCartTotals($_SESSION['usuario']['id'], $carrinho['id']);
          $_SESSION['carrinho']['total']['quantidade'] = empty($total['quantidade']) ? 0 : $total['quantidade'] ;
          $_SESSION['carrinho']['total']['total'] = empty($total['total']) ? 0 : $total['total'] ;
        }
      } else {
        $_SESSION['carrinho'] = $carrinho;
        $dados['cid'] = $carrinho['id'];
        $createitemcart = $carrinhos->createItemCart($dados, $combo_itens);
        if(substr($createitemcart, 0, 4) == 'erro'){
          $erro = substr($alteration, 4);
          switch (true) {
            case $erro == 0:
              echo json_encode(array('0', '<strong>Erro!</strong> Não há quantidade suficiente em stock para esse item!'));
              exit();
              break;
          }
        } else {
          $_SESSION['carrinho']['itens'] = $carrinhos->getCartItens($_SESSION['usuario']['id'], $carrinho['id']);
          $total = $carrinhos->getCartTotals($_SESSION['usuario']['id'], $carrinho['id']);
          $_SESSION['carrinho']['total']['quantidade'] = empty($total['quantidade']) ? 0 : $total['quantidade'] ;
          $_SESSION['carrinho']['total']['total'] = empty($total['total']) ? 0 : $total['total'] ;
        }
      }

      $minicart = $carrinhos->getMiniCartItens($_SESSION['usuario']['id'], $carrinho['id']);

      echo json_encode(array('1', json_encode($minicart), json_encode($_SESSION['carrinho']['total'])));
      exit();
    } else {
      echo json_encode(array('0', '<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Esse produto está esgotado ou desabilitado.'));
      exit();
    }

    //echo json_encode(array('1', '<i class="fas fa-check"></i><strong>Sucesso!</strong> Produto adicionado ao carrinho!'));

    // echo '<pre>';
    // print_r($_POST);
    // echo '</pre>';
    // echo '<pre>';
    // print_r($dados);
    // echo '</pre>';
    echo '<pre>';
    print_r($carrinho);
    echo '</pre>';
  }

  if($action == 'remove'){
    $alteration = $carrinhos->removeCartItem($_POST['dados'][0]['value']);
    if(substr($alteration, 0, 4) == 'erro'){
      $erro = substr($alteration, 4);
      switch (true) {
        case $erro == 1:
          echo json_encode(array('0', '<strong>Erro!</strong> Esse item não pode ser removido.'));
          exit();
          break;
      }
    } else {
      echo json_encode(array('1', $_POST['dados'][0]['value']));
      exit();
    }

    // echo '<pre>';
    // print_r($_POST);
    // echo '</pre>';
  }

  if($action == 'purge-cart'){

    $carrinhos->purgeCart($_SESSION['carrinho']['id']);
    $mensagem_erro = 'O seu carrinho foi esvaziado.';
    if ($_SESSION['cupom']) {
      $cupom->saveCupom($_SESSION['cupom']['id']);
      unset($_SESSION['cupom']);
    }
    unset($_SESSION['carrinho']);
    echo json_encode(array('1', 'O seu carrinho foi esvaziado.'));
    exit();

    // echo '<pre>';
    // print_r($_POST);
    // echo '</pre>';
  }

}

?>
