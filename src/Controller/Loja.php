<?php

$loader->get('src/Model/Produtos');
$prod = new Produtos(new Config());
$loader->get('src/Model/Categorias');
$cat = new Categorias(new Config());
$loader->get('src/Model/Marcas');
$marcas = new Marcas(new Config());
$loader->get('src/Model/Wishlist');
$wish = new Wishlist(new Config());

$module_data['hide_slide'] = 1;
$mensagem_erro = false;

if(Request::isAjax()){

  Resources::ajaxProtect($_POST, $_SESSION);

  $dados = array();
  foreach ($_POST['dados'] as $key => $value) {
    $dados[$value['name']] = $value['value'];
  }

  if($action == 'addWishlist'){
    if ($_SESSION['usuario']['logged']) {
      $wishlist_data['id_user'] = $_SESSION['usuario']['id'];
      $wishlist_data['id_prod'] = $dados['pid'];
      $wish->insertItem($wishlist_data);
      echo json_encode(array('1', '<strong>Sucesso!</strong> Adicionado à lista de desejos com sucesso!'));
      exit();
    }else {
      echo json_encode(array('0', '<strong>Erro!</strong> Precisa de fazer login para adicionar à lista de desejos!'));
      exit();
    }
  }

}

if($action == 'index'){

  $produtos = $prod->getStoreProducts($_SESSION['lg']);
  $module_data['produtos'] = $produtos;
  $module_data['actual_category'] = '';

  // echo '<pre>';
  // print_r($produtos);
  // echo '</pre>';
  // exit();

} else if($action == 'detalhe'){
  $module_data['transparent_menu'] = 1;
  $produto = $prod->getProductDetail($param, $_SESSION['lg']);
  $produto_variacoes = $prod->getProductVariations($produto['referencia'], $_SESSION['lg']);
  // $produtos_complementares = $prod->getProductDetail($param, $_SESSION['lg']);
  $produto_similares = $prod->getProductsStoreByCategorySlug($produto['categoria'], $_SESSION['lg'], 4);
  $module_data['produto'] = $produto;
  $module_data['variacoes'] = $produto_variacoes;
  $module_data['produtos_similares'] = $produto_similares;

  // echo '<pre>';
  // print_r($produto);
  // echo '</pre>';
  // echo '<pre>';
  // print_r($produto_similares);
  // echo '</pre>';
  // exit();

} else {

  $produtos = $prod->getProductsStoreByCategorySlug($action, $_SESSION['lg']);
  $module_data['produtos'] = $produtos;
  $module_data['actual_category'] = '';

  // echo '<pre>';
  // print_r($produtos);
  // echo '</pre>';
  // exit();

  // if (empty($param)) {
  //   $categoria = $cat->getCategoryBySlug($action);
  //   $produtos = $prod->getProductsByCategory($categoria['id']); //se for so action e so categoria
  // } else {
  //   $produtos = $prod->getProductsByMarcaAndCategory($action, $param); //se for action e param acction e marca e para categoria
  // }
  //
  // $module_data['produtos'] = $produtos;
  // $module_data['actual_category'] = $categoria['nome'];
}

 ?>
