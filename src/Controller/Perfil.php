<?php

use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\SMTP;
use PHPMailer\PHPMailer\Exception;

$loader->get('src/Model/Mail');

$loader->get('src/Model/Checkout');
$checkout = new Checkout(new Config());
$loader->get('src/Model/Pedidos');
$pedidos = new Pedidos(new Config());

$paises = $checkout->getAllowCountries();
$module_data['paises'] = $paises;

$module_data['hide_slide'] = 1;
$mensagem_erro = false;

if(Request::isAjax()){

  Resources::ajaxProtect($_POST, $_SESSION);

  if($action == 'login'){

    $dados = array();
    $dados['usuario'] = $_POST['dados'][0]['value'];
    $dados['senha'] = $_POST['dados'][1]['value'];

    $login_status = $users->login($dados);

    if(substr($login_status, 0, 4) == 'erro'){
      $erro = substr($login_status, 4);
      switch (true) {
        case $erro == 1:
          $usuario = $users->retUsuario($dados['usuario']);
          //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
          $html = '<h1>Link de redefinição de senha para a Iora Lingerie</h1>';
          $link_password = URL_BASE.'/user/redefinir_senha/'.md5($usuario['id']).md5(date('YmdH'));
          $html .= '<a href="'.$link_password.'">Clique redefinir a sua senha</a>';
          $html .= '<p>Atenção, este link tem a validade de apenas uma hora.</p>';

          //email de recuperação de senha
          SendMail::trySendMail('Recuperação de senha', $usuario['email'], $html, new PHPMailer(true));
          // echo '<pre>';
          // print_r($usuario);
          // echo '</pre>';
          echo json_encode(array('0', '<strong>Erro!</strong> O seu e-mail está registado mas não há senha, foi enviado um link para o seu e-mail para redefinir a senha (procure também na pasta de spam).'));
          exit();
          break;
        case $erro == 2:
          echo json_encode(array('0', '<strong>Erro!</strong> E-mail ou senha incorretos.'));
          exit();
          break;
      }
    } else {

      //se ja houver um carrinho em sessao
      if($_SESSION['carrinho']){
        //busca o usuario pelo email
        $usuario_login = $users->retUsuario(strip_tags($_POST['login'][0]['value']));
        //verifica se esse usuario já tem um carrinho
        $carrinho_usuario = $carrinhos->getCart($usuario_login['id']);
        if($carrinho_usuario){
          //marcando como removido carrinho antigo
          $carrinhos->purgeCart($carrinho_usuario['id']);
        }
        //muda a titularidade do carrinho atual para o usuario que logou agora
        $carrinhos->changeCartMaster($usuario_login['id'], $_SESSION['carrinho']['id']);
      }

      echo json_encode(array('1', '<strong>Sucesso!</strong> A redirecionar...'));
      exit();
    }
    exit();



  }

  if($action == 'cadastra'){

      for ($c = 0; $c < count($_POST['dados']); $c++) {
          if($_POST['dados'][$c]['value'] == ''){
              $falha = true;
              $message[$c] = ucfirst($_POST['dados'][$c]['name']).' está vazio';
          } else {
              $dados[$c] = $_POST['dados'][$c]['value'];
          }
      }

      // [0] => Renan Vilas
      // [1] => apelido
      // [2] => renan.pantoja@hotmail.com
      // [3] => tste
      // [4] => 935280135
      // [5] => 987654321
      // [6] => Av Almeida Garret
      // [7] => 4780-391
      // [8] => Santo Tirso
      // [9] => teste
      // [10] => 178
      // [11] => on
      // [12] => 4ebadb54007b93aadfaa531c0270319772766ab2b1c85af98adbbb9683600fdf


      // echo '<pre>';
      // print_r($dados);
      // echo '</pre>';

      if ($dados[11] != 'on') {
        echo json_encode(array('0', '<strong>Erro!</strong> Tem que aceitar os termos.'));
        exit();
      }

      if(!$falha){
        $cadastrar = $users->cadastrar($dados);
          if(substr($cadastrar, 0, 4) == 'erro'){
            $erro = substr($cadastrar, 4);
            switch (true) {
              case $erro == 1:
                $usuario = $users->retUsuario(strip_tags($_POST['dados'][0]['value']));
                //email------------------------------------------------------------------------------------------------------------------------------------------------------------------------
                $html = '<h1>Link de redefinição de senha para a Iora Lingerie</h1>';
                $link_password = URL_BASE.'/user/redefinir_senha/'.md5($usuario['id']).md5(date('YmdH'));
                $html .= '<a href="'.$link_password.'">Clique redefinir a sua senha</a>';
                $html .= '<p>Atenção, este link tem a validade de apenas uma hora.</p>';

                //email de recuperação de senha
                SendMail::trySendMail('Recuperação de senha', $usuario['email'], $html, new PHPMailer(true));
                // echo '<pre>';
                // print_r($usuario);
                // echo '</pre>';
                echo json_encode(array('0', '<strong>Erro!</strong> O seu e-mail está registado mas não há senha, foi enviado um link para o seu e-mail para redefinir a senha (procure também na pasta de spam).'));
                exit();
                break;
              case $erro == 2:
                echo json_encode(array('0', '<strong>Erro!</strong> Esse e-mail já está registado no nosso sistema.'));
                exit();
                break;
            }
          } else {
            $usuario = $users->retUsuario(strip_tags($_POST['dados'][2]['value']));
          }

          //se houver carrinho muda a titularidade para o usuario novo
          if($_SESSION['carrinho']){
            $carrinhos->changeCartMaster($usuario['id'], $_SESSION['carrinho']['id']);
          }

          //força login no sistema com novo usuário
          $users->force_login($usuario);

          echo json_encode(array('1', '<strong>Sucesso!</strong> Utilizador criado.'));
          exit();
          /*$msg = 'Obrigado por se registar no Sistema de Pedidos da Iora Lingerie. Clique no link para confirmar o seu e-mail. /n https:///userverification/index/'.md5($pai);
          if(Resources::sendMail($dados[0],$dados, 'Novo Utilizador', $msg)){
              echo json_encode(array('1', '<strong>Sucesso!</strong> Utilizador criado, verifique o seu e-mail!',$pai));
          } else {
              echo json_encode(array('0', '<strong>Erro!</strong> Não conseguimos enviar-lhe e-mails, entre em contacto.',$pai));
          }*/
      } else {
          echo json_encode(array('0', '<strong>Erro!</strong> '.implode(', ', $message).'.'));
          exit();
      }
    }

    if($action == 'atualiza'){

      // [0] => Renan
      // [1] => Vilas
      // [2] => testenovo@teste.com
      // [3] => 987 // senha
      // [4] => 935280135 // tel
      // [5] => 987654321 // nif
      // [6] => Av Almeida Garret
      // [7] => 4780391 // cp
      // [8] => Santo Tirso
      // [9] => porto
      // [10] => 178 // pais
      // [11] => 33 // uid

      for ($c = 0; $c < count($_POST['dados']); $c++) {
          if($_POST['dados'][$c]['value'] == ''){
              $falha = true;
              $message[$c] = ucfirst($_POST['dados'][$c]['name']).' está vazio';
          } else {
              $dados[$c] = $_POST['dados'][$c]['value'];
          }
      }

      // echo '<pre>';
      // print_r($dados);
      // echo '</pre>';

      $user_update = $users->updateUsuario($dados, $_SESSION['usuario']['id']);
      if ($user_update) {
        echo json_encode(array('1', '<strong>Sucesso!</strong> Utilizador atualizado.'));
        exit();
      } else {
        echo json_encode(array('0', '<strong>Erro!</strong> Algo deu errado.'));
        exit();
      }
    }


}

if($action == 'index'){

  if(empty($_SESSION['usuario']['logged'])){ header('Location: '.URL_BASE.'/perfil/login'); }
  $usuario = $users->readUsuario(null, $_SESSION['usuario']['id']);
  $orders = $pedidos->getUserOrders($_SESSION['usuario']['id']);
  $module_data['usuario'] = $usuario;
  $module_data['pedidos'] = $orders;

  // echo '<pre>';
  // print_r($orders);
  // echo '</pre>';

} elseif ($action == 'encomenda') {
  $module_data['pedido'] = $pedidos->getOrdersDetails($param, $_SESSION['lg']);

  if ($module_data['pedido'][0]['status'] == 1) {
    $bank = $checkout->getPaymentMethodsById(1);
    $module_data['info'] = $bank['instructions'];
    $module_data['info'] .= implode('<br>', json_decode($bank['instructions'], true));
  }

  // echo '<pre>';
  // print_r($module_data['pedido']);
  // echo '</pre>';

} elseif ($action == 'login') {

} elseif ($action == 'criar_conta') {

} elseif ($action == 'logout') { $users->logout(); header('Location: '.URL_BASE.'/loja'); }

 ?>
