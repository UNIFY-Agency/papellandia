<?php
/**
 * Created by Unify.
 * User: Renan Pantoja
 * Date: 05/08/20
 * Time: 15:17
 */

 // ini_set('display_errors', 1);
 // ini_set('display_startup_errors', 1);
 // error_reporting(E_ALL);

// Hide all errors
error_reporting(0);
ini_set('display_errors', 0);

// Show all errors
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

session_start();

header('Content-Type: text/html; charset=utf8');

setlocale(LC_ALL, 'Portuguese_Portugal.1252');
date_default_timezone_set('Europe/Lisbon');

define('DS', DIRECTORY_SEPARATOR);
define('ROOT', pathinfo(__FILE__)['dirname']);

include 'lib/Loader.php';

$loader = Loader::get_instance(ROOT, DS);
$loader->get('lib/Request');
$loader->get('lib/GeoIp');
$loader->get('lib/Register_access');
$loader->get('lib/Mobile_Detect');
$loader->get('lib/PHPMailer/src/Exception');
$loader->get('lib/PHPMailer/src/PHPMailer');
$loader->get('lib/PHPMailer/src/SMTP');
$loader->get('config/Config');
$loader->get('config/Route');
$loader->get('src/Model/Resources');
$loader->get('src/Model/Cart');
$loader->get('src/Model/Users');
$loader->get('src/Model/Log');
$loader->get('lib/twig/autoload');

$geo = new GeoIp;
if(empty($_SESSION['usuario']['id'])){
  $_SESSION['usuario']['id'] = md5($geo->getIP()).md5($_SERVER['HTTP_USER_AGENT']); // this should *never* change during a session
  //registrando acesso do usuario
  $register = new Register(new Config());
  $user_os = Register::getOS($_SERVER['HTTP_USER_AGENT']);
  $register->registerAccess($user_os, $geo->getIP());
}

if(empty($_SESSION['lg'])){
  $loader->get('src/Controller/Language'); //module de idiomas, seta o idioma default
}

if(empty($_SESSION['cookies'])){
  $_SESSION['cookies'] = 0; //verifica se foi aceitado os cokioes para n exibir mais
}

if(empty($_SESSION['location'])){
  $_SESSION['location'] = '';
}

if(isset($_GET['q'])){
  header('Location: '.URL_BASE.'/pesquisa/index/'.$_GET['q']); //verifica se foi realizado pesquisa
}

$users = new Users(new Config());
$carrinhos = new Cart(new Config());
$log = new Log(new Config());

$carrinhos->refleshCart($_SESSION['usuario']['id'], $_SESSION['lg']);

$t_loader = new Twig_Loader_Filesystem('src/View');
$twig = new Twig_Environment($t_loader, [
    'debug' => true,
    // ...
]);
$twig->addExtension(new \Twig\Extension\DebugExtension());
$twig->getExtension(\Twig\Extension\CoreExtension::class)->setTimezone('Europe/Lisbon');
$device = new Mobile_Detect;

$config = new Config();

//forçar redirecionamento para HTTPS
// if(empty($_SERVER['HTTPS'])){ header('Location: https://'.$_SERVER['HTTP_HOST']); } else { $protocol = 'https'; }
$protocol = 'http';

$rota = Route::dynamicUrl($protocol . '://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'], 2);
define("URL_BASE", array_pop($rota));

$module = (empty($rota[0])) ? 'Site' : ucfirst($rota[0]);
$action = (isset($rota[1]) && (!empty($rota[1]))) ? $rota[1] : 'index';
$param = (isset($rota[2])) ? $rota[2] : null ;

// if($geo->getIP() != '94.61.211.253'){ exit();}

$ips = array('94.61.211.253');

if(in_array($geo->getIP(), $ips)){
// echo URL_BASE.'<br>';
// echo URL_BASE.'<br>';print_r($rota);
// echo $module.'<br>';
// echo $action.'<br>';
// echo $param.'<br>';

// echo '<pre>';
// print_r($_SESSION);
// echo '</pre>';

} else {
  // include_once 'src'.DS.'View'.DS.'Includes'.DS.'maintenance.php';
  // exit();
}

//if(file_exists(include 'src'.DS.'Model'.DS.$module . '.php')) include 'src'.DS.'Model'.DS.$module . '.php';
if(file_exists('src'.DS.'Model'.DS.$module . '.php')) include_once 'src'.DS.'Model'.DS.$module . '.php';
if(file_exists('src'.DS.'Controller'.DS.$module . '.php')) include_once 'src'.DS.'Controller'.DS.$module . '.php';

if (!Request::isAjax()){
    include_once 'src'.DS.'View'.DS.'view_build.php';
}
