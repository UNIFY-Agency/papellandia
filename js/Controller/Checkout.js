
paypal.Buttons({
  style:{
    color:'blue',
    shape:'pill'
  },
  createOrder:function(data,actions){
    return actions.order.create({
      purchase_units:[{
        amount:{
          currency: 'EUR',
          value: parseFloat($('.total').html().replace('€', '')).toFixed(2)
        }
      }]
    });
  },
  onApprove:function(data,actions) {
    return actions.order.capture().then(function(details){
      console.log(details);
      console.log(details.status);
      // create_time: "2021-03-25T12:22:15Z"
      // id: "80W54204BG9157118"
      // intent: "CAPTURE"
      // links: [{…}]
      // payer: {email_address: "sb-p3bw15545566@personal.example.com", payer_id: "UET6H3XKH4KQN", address: {…}, name: {…}}
      // purchase_units: [{…}]
      // status: "COMPLETED"
      // update_time: "2021-03-25T12:22:32Z"

      if (details.status == 'COMPLETED') {
        $('#paypal-btn').html('PAGO: '+details.id);

        var dados = $('#place-order-form').serializeArray();
        dados.push({name: "pago", value: details.id});
        dados.push({name: "user_session_id", value: USER_SID});
        console.log(dados);

        $.ajax({
              type: "POST",
              url: URL_BASE + '/Checkout/makeOrder',
              data: {dados},
              success: function (result) {
                  console.log(result);
                  var results = jQuery.parseJSON(result);
                  if(results[0] == 0) {
                      $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn();
                      $("html, body").animate({ scrollTop: 0 }, "slow");
                  } else {
                      $("#notifications").removeClass().addClass('alert alert-success').html(results[1]).fadeIn();
                      window.location.replace(URL_BASE+'/loja/concluido');
                  }
              }
          });

        // window.location.replace();
      } else {
        $("#notifications").removeClass().addClass('alert alert-danger').html('<strong>Erro!</strong> Houve um problema com seu pagamento!').fadeIn().delay(5000).fadeOut();
        $("html, body").animate({ scrollTop: 0 }, "slow");
      }
    })
  },
  onCancel:function(data){
    $("#notifications").removeClass().addClass('alert alert-danger').html('<strong>Erro!</strong> Pagamento cancelado!').fadeIn().delay(5000).fadeOut();
    $("html, body").animate({ scrollTop: 0 }, "slow");
    // window.location.replace(URL_BASE + '/cart');
  }
}).render('#paypal-btn');



$('.form-check-input').click( function() {
  // $('#paypal-btn').show('slide');

  var serialized = $('input[name!=nota]', '#place-order-form').serialize();
  if(serialized.indexOf('=&') > -1 || serialized.substr(serialized.length - 1) == '='){
   // console.log('Ainda há compos vazios');
   $('.form-check-input').prop('checked', false);
   $("#notifications").removeClass().addClass('alert alert-danger').html('<strong>Erro!</strong> Não pode fazer o pagamento antes de preencher os dados!').fadeIn().delay(5000).fadeOut();
   $("html, body").animate({ scrollTop: 0 }, "slow");
 } else {
   if($('.paypal-show').is(':checked')) {
     $('#paypal-btn').show('slide');
   } else {
     $('#paypal-btn').hide('slide');
   }
 }
});


$('#place-order-btn').click( function() {
  validaForm('#place-order-form');
  $('#place-order-form-submit').trigger('click');
});

$("#show-nif").click( function(){
   if($(this).is(':checked')){
     $('.nif-div').fadeIn();
   } else {
     $('.nif-div').fadeOut();
     $('#nif').val(0);
   }
});

$("#nif-ref").focusout(function(){
  var nif = $("#nif-ref").val();
  $("#nif").val(nif);
  console.log($("#nif-ref").val());
});

$("#pagamento2").click( function(){
   if($(this).is(':checked')){
     $('.troco-div').fadeIn();
     $('#pagamento').val(2);
   } else {
     $('.troco-div').fadeOut();
     $('#troco').val(0);
   }
});

$("#pagamento1").click( function(){
   if($(this).is(':checked')){
     $('.troco-div').fadeOut();
     $('#troco').val(0);
     $('#pagamento').val(1);
   } else {
     $('.troco-div').fadeIn();
   }
});

$("#entrega2").click( function(){
   if($(this).is(':checked')){
     $('#entrega').val(2);
     var remove_tax = parseFloat(parseFloat($('.total_amount').html()) - parseFloat($('.shipping td').html()));
     if(remove_tax >= parseFloat($('.total_amount').attr('min'))){
        $('.total_amount').html(remove_tax.toFixed(2) + '€');
        $('.shipping td').hide();
     }
     //console.log(remove_tax);
   }
});

$("#entrega1").click( function(){
   if($(this).is(':checked')){
     $('#entrega').val(1);
     var remove_tax = parseFloat(parseFloat($('.total_amount').html()) + parseFloat($('.shipping td').html()));
     if(remove_tax <= parseFloat($('.total_amount').attr('max'))){
       $('.total_amount').html(remove_tax.toFixed(2) + '€');
       $('.shipping td').show();
     }
   }
});

$("#troco-ref").focusout(function(){
  var troco = $("#troco-ref").val();
  $("#troco").val(troco);
  console.log($("#troco-ref").val());
});

$('.cancel-pre-order').click( function(){

  var dados = [];
  dados.push({name: "user_session_id", value: USER_SID});

  $.post( URL_BASE + '/carrinho/cancel-pre-order', {dados}, function( result ) {
    console.log(result);
    location.reload();
  });
});

$("#place-order-form").submit(function( event ) {
  event.preventDefault();

  var btn = $('#place-order-btn');
  btn.prop('disabled', true);
  $(btn).val($(btn).attr("data-loading-text"));
  setTimeout(function(){
      btn.prop('disabled', false);
      $(btn).val('Continuar');
  }, 3*2000);

  var dados = $(this).serializeArray();
  dados.push({name: "user_session_id", value: USER_SID});

  console.log(dados);
  console.log($('input[name=payment-option]:checked', '#myForm').val());

  $.ajax({
        type: "POST",
        url: URL_BASE + '/Checkout/makeOrder',
        data: {dados},
        success: function (result) {
            console.log(result);
            var results = jQuery.parseJSON(result);
            if(results[0] == 0) {
                $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn();
                $("html, body").animate({ scrollTop: 0 }, "slow");
            } else {
                $("#notifications").removeClass().addClass('alert alert-success').html(results[1]).fadeIn();
                // $("#redirect-link").html(results[2]);
                // console.log('feito : ' + results[2]);
                window.location.replace(URL_BASE+'/loja/concluido');
            }
        }
    });

});


$('#entrega-select').on('change', function() {
  console.log('change');
  var dados = [];
  dados.push({name: "pais_id", value: $( "#pais-porte option:selected" ).attr('pid')});
  dados.push({name: "user_session_id", value: USER_SID});

  $.ajax({
        type: "POST",
        url: URL_BASE + '/checkout/getPortRule',
        data: {dados: dados},
        success: function (result) {
          console.log(result);
          var results = jQuery.parseJSON(result);
          if(results[0] == 0) {
              $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
              $("html, body").animate({ scrollTop: 0 }, "slow");
          } else {
            var entrega = parseFloat($( "#entrega-select option:selected" ).attr('tax'));
            var portes = parseFloat($( "#pais-porte option:selected" ).val());
            var subtotal = parseFloat($( ".subtotal" ).html().replace(/[^0-9\.]/g, ''));
            // console.log(rollRules(jQuery.parseJSON(results[1]), portes, subtotal));
            var finals = rollRules(jQuery.parseJSON(results[1]), portes, subtotal, entrega);
            if ($('.cupom').attr('desc')){
              var cupom = cupomCalc(parseInt($('.cupom').attr('tipo')), parseFloat($('.cupom').attr('desc')), finals[1]);
              $('.porte').html(finals[0].toFixed(2) + '€');
              $('.total').html(cupom.toFixed(2)  + '€');
            } else {
              $('.porte').html(finals[0].toFixed(2) + '€');
              $('.total').html(finals[1].toFixed(2)  + '€');
            }
          }
        }
    });
});

$('#pais-porte').on('change', function() {
  console.log('change');
  var dados = [];
  dados.push({name: "pais_id", value: $( "#pais-porte option:selected" ).attr('pid')});
  dados.push({name: "user_session_id", value: USER_SID});

  $.ajax({
        type: "POST",
        url: URL_BASE + '/checkout/getPortRule',
        data: {dados: dados},
        success: function (result) {
          console.log(result);
          var results = jQuery.parseJSON(result);
          if(results[0] == 0) {
              $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
              $("html, body").animate({ scrollTop: 0 }, "slow");
          } else {
            var entrega = parseFloat($( "#entrega-select option:selected" ).attr('tax'));
            var portes = parseFloat($( "#pais-porte option:selected" ).val());
            var subtotal = parseFloat($( ".subtotal" ).html().replace(/[^0-9\.]/g, ''));
            // console.log(rollRules(jQuery.parseJSON(results[1]), portes, subtotal));
            var finals = rollRules(jQuery.parseJSON(results[1]), portes, subtotal, entrega);
            if ($('.cupom').attr('desc')){
              var cupom = cupomCalc(parseInt($('.cupom').attr('tipo')), parseFloat($('.cupom').attr('desc')), finals[1]);
              $('.porte').html(finals[0].toFixed(2) + '€');
              $('.total').html(cupom.toFixed(2)  + '€');
            } else {
              $('.porte').html(finals[0].toFixed(2) + '€');
              $('.total').html(finals[1].toFixed(2)  + '€');
            }
          }
        }
    });
});

//pega o pais no carregamento da pagina para atualizar valor de entrega1
var dados = [];
dados.push({name: "pais_id", value: $( "#pais-porte option:selected" ).attr('pid')});
dados.push({name: "user_session_id", value: USER_SID});
$.post( URL_BASE + '/checkout/getPortRule', {dados}, function( result ) {
  // console.log(result);
  var results = jQuery.parseJSON(result);
  if(results[0] == 0) {
      $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
      $("html, body").animate({ scrollTop: 0 }, "slow");
  } else {
    var entrega = parseFloat($( "#entrega-select option:selected" ).attr('tax'));
    var portes = parseFloat($( "#pais-porte option:selected" ).val());
    var subtotal = parseFloat($( ".subtotal" ).html().replace(/[^0-9\.]/g, ''));
    // console.log(rollRules(jQuery.parseJSON(results[1]), portes, subtotal));
    var finals = rollRules(jQuery.parseJSON(results[1]), portes, subtotal, entrega);
    if ($('.cupom').attr('desc')){
      var cupom = cupomCalc(parseInt($('.cupom').attr('tipo')), parseFloat($('.cupom').attr('desc')), finals[1]);
      $('.porte').html(finals[0].toFixed(2) + '€');
      $('.total').html(cupom.toFixed(2)  + '€');
    } else {
      $('.porte').html(finals[0].toFixed(2) + '€');
      $('.total').html(finals[1].toFixed(2)  + '€');
    }
  }
});

function cupomCalc(tipo, valor, total){
  if(tipo == 1){
    var total = parseFloat(total - ((total * valor) / 100));
    return total;
  } else if(tipo == 2){
    var total = parseFloat(total - valor);
    return total;
  }
}

function rollRules(rules, portes, subtotal, entrega){
  if (entrega == 0){
    var portes = 0;
  }
  if(rules.length > 0 && portes > 0) {
    for (var i = 0; i < rules.length; i++) {
      // console.log(rules[i]);
      switch (parseInt(rules[i]['operador'])) {
        case 0:
          if (subtotal < rules[i]['variavel']) {
            if (rules[i]['modificador'] == 1) {
              var portes_final = parseFloat(portes + ((portes/rules[i]['porcentagem']) * 100));
              var valor_final = portes_final + subtotal;
              var finals = [portes_final, valor_final];
              return finals;
            } else {
              var portes_final = parseFloat(portes - ((portes/rules[i]['porcentagem']) * 100));
              var valor_final = portes_final + subtotal;
              var finals = [portes_final, valor_final];
              return finals;
            }
          } else {
            var portes_final = portes;
            var valor_final = portes + subtotal;
            var finals = [portes_final, valor_final];
            return finals;
          }
          break;
        case 1:
          if (subtotal == rules[i]['variavel']) {
            if (rules[i]['modificador'] == 1) {
              var portes_final = parseFloat(portes + ((portes/rules[i]['porcentagem']) * 100));
              var valor_final = portes_final + subtotal;
              var finals = [portes_final, valor_final];
              return finals;
            } else {
              var portes_final = parseFloat(portes - ((portes/rules[i]['porcentagem']) * 100));
              var valor_final = portes_final + subtotal;
              var finals = [portes_final, valor_final];
              return finals;
            }
          } else {
            var portes_final = portes;
            var valor_final = portes + subtotal;
            var finals = [portes_final, valor_final];
            return finals;
          }
          break;
        case 2:
          if (subtotal > rules[i]['variavel']) {
            if (rules[i]['modificador'] == 1) {
              var portes_final = parseFloat(portes + ((portes/rules[i]['porcentagem']) * 100));
              var valor_final = portes_final + subtotal;
              var finals = [portes_final, valor_final];
              return finals;
            } else {
              var portes_final = parseFloat(portes - ((portes/rules[i]['porcentagem']) * 100));
              var valor_final = portes_final + subtotal;
              var finals = [portes_final, valor_final];
              return finals;
            }
          } else {
            var portes_final = portes;
            var valor_final = portes + subtotal;
            var finals = [portes_final, valor_final];
            return finals;
          }
          break;
        default:
          var portes_final = portes;
          var valor_final = portes + subtotal;
          var finals = [portes_final, valor_final];
          return finals;
      }
      if(portes <= 0) { break; }
    }
  } else {
    var portes_final = portes;
    var valor_final = portes + subtotal;
    var finals = [portes_final, valor_final];
    return finals;
  }
}

$("#form-validade-cupom").submit(function( event ) {
  event.preventDefault();

  var btn = $('#form-validade-cupom-submit');
  btn.prop('disabled', true);
  $(btn).val($(btn).attr("data-loading-text"));
  setTimeout(function(){
      btn.prop('disabled', false);
      $(btn).val('Usar Cupom');
  }, 3*2000);

  var dados = $(this).serializeArray();
  dados.push({name: "user_session_id", value: USER_SID});

  console.log(dados);

  $.post( URL_BASE + '/checkout/get-cupom', {dados}, function( result ) {
      console.log(result);
      var results = jQuery.parseJSON(result);
      if(results[0] == 0) {
          $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
          $("html, body").animate({ scrollTop: 0 }, "slow");
      } else {
          // alert('Cupom Adicionado');
          window.location.replace(URL_BASE + '/checkout');
      }
  });

});

$(document).on('click', '.remove-cupom', function() {

  var dados = [];
  dados.push({name: "user_session_id", value: USER_SID});

  $.post( URL_BASE + '/checkout/remove-cupom', {dados}, function( result ) { window.location.replace(URL_BASE + '/checkout'); });

});


function validaForm(form){
  $(form).validate({
    onkeyup: false,
    onclick: false,
    onfocusout: false,
    rules: {
      'captcha': {
        captcha: true
      },
      'checkboxes[]': {
        required: true
      },
      'radios': {
        required: true
      }
    },
    errorPlacement: function(error, element) {
      if (element.attr('type') == 'radio' || element.attr('type') == 'checkbox') {
        error.appendTo(element.closest('.form-group'));
      } else {
        error.insertAfter(element);
      }
      return false;
    }
  });
  return true;
}
