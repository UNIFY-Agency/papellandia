$('.add-prod-cart').click( function(){
  // var pid = $(this).attr('pid');
  // var lid = $(this).attr('loja');
  // $('.float-cart').show('fade');
  var lid = 1;

  var pid = $('.variacao-value.active').find('input').val();
  console.log(pid);
  if (pid === undefined) {
    $("#notifications").removeClass().addClass('alert alert-danger').html('<i class="fas fa-exclamation-triangle"></i><strong>Erro!</strong> Nenhum tamanho selecionado').fadeIn().delay(5000).fadeOut();
    $("html, body").animate({ scrollTop: 0 }, "slow");
  }
  else {

    var dados = [];
    dados.push({name: "pid", value: pid});
    dados.push({name: "lid", value: lid});
    dados.push({name: "min", value: null});
    dados.push({name: "max", value: null});
    dados.push({name: "user_session_id", value: USER_SID});

    console.log(dados);

    $.ajax({
          type: "POST",
          url: URL_BASE + '/cart/add',
          data: {dados: dados},
          success: function (result) {
              // console.log(result);
              var results = jQuery.parseJSON(result);
              if(results[0] == 0) {
                  $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
                  $("html, body").animate({ scrollTop: 0 }, "slow");
              } else {
                  var prod = jQuery.parseJSON(results[1]);
                  //console.log(prod);
                  var total = jQuery.parseJSON(results[2]);
                  //console.log(total);
                  // $('.cart-qty').html(total['quantidade'] - 1); // -1 por causa do hashi que sempre é criado junto no cart
                  // $('.cart-qty').show();
                  // $('.cart-qty').html(total['quantidade']);
                  // $('.cart-total .price').html(total['total']);
                  // $('.mini-products-list').html('');

                  // for (var i = 0; i < prod.length; i++) {
                  //   var cart_line = '<li class="single-product-cart" id="cartline-'+prod[i]['id']+'"><div class="cart-img"><a href="javascript:void(0);"><img src="'+URL_BASE+'/bo/assets/upload/'+prod[i]['image_url']+'" alt=""></a></div><div class="cart-title"><h4><a href="javascript:void(0);">'+prod[i]['nome']+'</a></h4><span>'+prod[i]['quantidade']+'</span> x <span>'+prod[i]['preco']+'</span></div><div class="cart-delete"><a href="javascript:void(0);" class="remove-item-cart" iid="'+prod[i]['id']+'">×</a></div></li>';
                  //   $('.mini-products-list').append(cart_line);
                  // }

                  // $('.cart-bag-circle').css({'width' : '2em', 'height' : '2em', 'font-size' : '7em', 'padding-top' : '1em'});
                  // setTimeout(function(){
                  //   $('.cart-bag-circle').css({'width' : '20px', 'height' : '20px', 'font-size' : '10px', 'padding-top' : '0'});
                  // }, 800);

                  // $('#headerTopCartDropdown').addClass('show');

                  //$("#notifications").removeClass().addClass('alert alert-success').html(results[1]).fadeIn().delay(2000).fadeOut();
                  //window.location.replace(URL_BASE + '/sys');
                  window.location.replace(URL_BASE + '/cart');
              }
          }
      });
    }
});
