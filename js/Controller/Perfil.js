$("#frmSignUp").on('submit',(function(e) {
    e.preventDefault();

    var btn = $('#submit_frmSignUp');
    btn.prop('disabled', true);
    $(btn).val($(btn).attr("data-loading-text"));
    setTimeout(function(){
        btn.prop('disabled', false);
        $(btn).val('Registrar');
    }, 5*1000);

    var dados = $("#frmSignUp").serializeArray();
     console.log(dados);

    dados.push({name: "user_session_id", value: USER_SID});

/*  0: {name: "nome", value: "Renan Vilas"}
    1: {name: "apelido", value: "teste"}
    2: {name: "email", value: "renan.pantoja@hotmail.com"}
    3: {name: "senha", value: "teste"}
    4: {name: "tel", value: "935280135"}
    5: {name: "nif", value: "teste"}
    6: {name: "morada", value: "Av Almeida Garret"}
    7: {name: "cp", value: "4780-391"}
    8: {name: "localidade", value: "Santo Tirso"}
    9: {name: "distrito", value: "teste"}
    10: {name: "pais", value: "1"}
    11: {name: "termos", value: "on"} */
    /*if(dados[1]['value'] == '' || dados[2]['value'] == ''){
      //$("#cadastro-notifications").removeClass().html('<strong>Erro!</strong> O Campo senha não pode ser vazio!').addClass('alert alert-danger').fadeIn().delay(2000).fadeOut();
      senha = false;
    }*/

    //if(dados[1]['value'] == 2){ window.location.replace(URL_BASE + '/site/empresasign'); senha = false; }

    $.ajax({
        type: "POST",
        url: URL_BASE + '/Perfil/cadastra',
        data: {dados: dados},
        success: function (result) {
            console.log(result);
            var results = jQuery.parseJSON(result);
            if(results[0] == 0) {
                $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
                $('#frmSignUp')[0].reset();
            } else {
                $("#notifications").removeClass().addClass('alert alert-success').html(results[1]).fadeIn().delay(2000).fadeOut();
                $('#frmSignUp')[0].reset();
                window.location.replace(URL_BASE + '/perfil');
            }

        }
    });

}));

$("#frmSignIn").on('submit',(function(e) {
    e.preventDefault();

    var btn = $('#submit_frmSignIn');
    btn.prop('disabled', true);
    $(btn).val($(btn).attr("data-loading-text"));
    setTimeout(function(){
        btn.prop('disabled', false);
        $(btn).val('Login');
    }, 3*1000);

    var dados = $("#frmSignIn").serializeArray();
    dados.push({name: "user_session_id", value: USER_SID});

    console.log(dados);

    // {name: "email", value: "teste5@teste.com"}
    // {name: "passwordlog", value: "teste"}
    // {name: "user_session_id", value: "4ebadb54007b93aadfaa531c0270319772766ab2b1c85af98adbbb9683600fdf"}

    //$.post( URL_BASE + '/src/Controller/User_ajax.php', {login}, function( result ) {
    $.post( URL_BASE + '/Perfil/login', {dados}, function( result ) {
        console.log(result);
        var results = jQuery.parseJSON(result);
        if(results[0] == 0) {
          $("#notifications").removeClass().addClass("alert alert-danger").html(results[1]).fadeIn().delay(4000).fadeOut();
          $("#frmSignIn input[name=passwordlog]").val('');
        } else {
          $("#notifications").removeClass().addClass("alert alert-success").html(results[1]).fadeIn().delay(2000).fadeOut();
          window.location.replace(URL_BASE + '/perfil');
        }
    });

}));

$("#frmEdit").on('submit',(function(e) {
    e.preventDefault();

    var btn = $('.save-button');
    btn.prop('disabled', true);
    $(btn).val($(btn).attr("data-loading-text"));
    setTimeout(function(){
        btn.prop('disabled', false);
        $(btn).val('Guardar alterações');
    }, 5*1000);

    var dados = $("#frmEdit").serializeArray();
    dados.push({name: "user_session_id", value: USER_SID});
    console.log(dados);

/*  0: {name: "nome", value: "Renan"}
    1: {name: "apelido", value: "Vilas"}
    2: {name: "email", value: "testenovo@teste.com"}
    3: {name: "senha", value: ""}
    4: {name: "tel", value: "935280135"}
    5: {name: "nif", value: "987654321"}
    6: {name: "morada", value: "Av Almeida Garret"}
    7: {name: "cp", value: "4780391"}
    8: {name: "localidade", value: "Santo Tirso"}
    9: {name: "distrito", value: "porto"}
    10: {name: "pais", value: "178"}
    11: {name: "user_session_id", value: "33"}
    */

    if(dados[3].value == ''){ // caso de senha vazia
      dados[3].value = 0;
    }

    //if(dados[1]['value'] == 2){ window.location.replace(URL_BASE + '/site/empresasign'); senha = false; }

    $.ajax({
        type: "POST",
        url: URL_BASE + '/Perfil/atualiza',
        data: {dados: dados},
        success: function (result) {
            console.log(result);
            var results = jQuery.parseJSON(result);
            if(results[0] == 0) {
                $("#notifications-perfil").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
                $('#frmEdit')[0].reset();
            } else {
                $("#notifications-perfil").removeClass().addClass('alert alert-success').html(results[1]).fadeIn().delay(2000).fadeOut();
                // setTimeout(function(){ location.reload(); }, 1000);
            }

        }
    });
}));
