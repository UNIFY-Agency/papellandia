$('.catalogo-item').click( function(){

  var img_url = $(this).find('.img-fluid').attr('src');
  var catalogo_id = $(this).attr('cid');

  // console.log(img_url);

  $('.catalogo-logo').attr('src', img_url);
  $('#cid').val(catalogo_id);

});

$("#orcamentoForm").on('submit',(function(e) {
    e.preventDefault();

    var btn = $('#send-catalog-submit');
    btn.prop('disabled', true);
    $(btn).html($(btn).attr("data-loading-text"));
    setTimeout(function(){
        btn.prop('disabled', false);
        $(btn).html('<i class="far fa-envelope mr-2"></i>Enviar</button>');
    }, 3*2000);

    var dados = $("#send-catalog").serializeArray();
    dados.push({name: "user_session_id", value: USER_SID});

    console.log(dados);

    $.post( URL_BASE + '/Catalogo/send', {dados}, function( result ) {
    //     console.log(result);
    //     var results = jQuery.parseJSON(result);
    //     if(results[0] == 0) {
    //       $("#notifications").removeClass().addClass("alert alert-danger").html(results[1]).fadeIn().delay(4000).fadeOut();
    //       document.getElementById("send-catalog").reset();
    //     } else {
    //       $("#notifications").removeClass().addClass("alert alert-success").html(results[1]).fadeIn().delay(2000).fadeOut();
    //       document.getElementById("send-catalog").reset();
    //     }
    });

}));
