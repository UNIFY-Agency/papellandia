jQuery(document).ready(function() {
  //idioma
  jQuery('.lang').click(function() {
    // console.log(jQuery(this).attr('lang'));
    var url = URL_BASE + '/language/change/' + jQuery(this).attr('lang');
    jQuery.post(url, function() {
      location.reload();
    });
  });

  jQuery('.cookie-ok').click(function(){
      //console.log(jQuery(this).attr('class'));
      var url = URL_BASE + '/cookies/allow/';
      jQuery.post( url, function(){ jQuery('.notice-bottom-bar').hide(); });
  });

  jQuery('.header-search-mobile').click(function() {
    console.log('clique');
    jQuery('.search-line').toggle("slow");
  });

  $('.dropdown-mega-sub-title').click(function() {

    // console.log($(this).attr('class'));

    if (!$(this).hasClass('active-menu')) {
      $(".dropdown-mega-sub-title").removeClass("active-menu").find('.arrowdown-active').removeClass().addClass('arrowdown').attr('src', URL_BASE + '/assets/img/icons/arrowdown.svg');
      $(this).addClass('active-menu').find('.arrowdown').removeClass().addClass('arrowdown-active').attr('src', URL_BASE + '/assets/img/icons/arrowdown-active.svg');
      // console.log('n tem');
    }

  });



  $('.float-search-open').click(function() {
    if($('.float-search').is(':visible')){
      $('.float-search').hide("slide", {
        direction: 'left'
      }, 500);
    } else {
      $('.float-search').show("slide", {
        direction: 'left'
      }, 500);
    }
  });

  $('.float-search-close').click(function() {
    $('.float-search').hide("slide", {
      direction: 'left'
    }, 500);
  });



  $('.hamburguer-mobile').click(function() {
    $('.float-menu-mobile').show("slide", {
      direction: 'left'
    }, 500);
  });
  $('.menu-mobile-close').click(function() {
    console.log('clique');
    $('.float-menu-mobile').hide("slide", {
      direction: 'left'
    }, 500);
  });

  $(document).scroll(function() {
         if($(window).scrollTop() > 300){

          $(".container-menu-mobile").css({"position": "fixed", "background": "Gainsboro"});

        }else if($(window).scrollTop() < 350){

          $(".container-menu-mobile").css({"position": "relative", "background": "transparent"});

         }
  });






  $(document).on('click', '.remove-item-cart', function() {
  //$('.remove-item-cart').on('click', function(){
    var iid = $(this).attr('iid');

    console.log(iid);

    var dados = [];
    dados.push({name: "iid", value: iid});
    dados.push({name: "user_session_id", value: USER_SID});

    $.ajax({
          type: "POST",
          url: URL_BASE + '/cart/remove',
          data: {dados: dados},
          success: function (result) {
              console.log(result);
              var results = jQuery.parseJSON(result);
              if(results[0] == 0) {
                  $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
                  $("html, body").animate({ scrollTop: 0 }, "slow");
              } else {
                  var li_id = '#cartline-'+results[1];
                  var subtrai_price = $('.cart-total .price').html() - ($(li_id + ' span:last-child').html() * $(li_id + ' span').html());
                  var subtrai_qty = $('.cart-qty').html() - $(li_id + ' span').html();
                  console.log(subtrai_price.toFixed(2));

                  $('.cart-total .price').html(subtrai_price.toFixed(2));
                  $('.cart-qty').html(subtrai_qty);

                  $(li_id).remove();
              }
          }
      });
  });

  // $('.plus').click( function(){
  //   var lid = $(this).parent().parent().parent().attr('id');
  //   console.log(lid);
  //   var input_value = $(this).parent().find('input[name="quantity"]').val();
  //
  //   var dados = [];
  //   dados.push({name: "lid", value: lid});
  //   dados.push({name: "user_session_id", value: USER_SID});
  //
  //   $.post( URL_BASE + '/cart/plus', {dados}, function( result ) {
  //       console.log(result);
  //       var results = jQuery.parseJSON(result);
  //       if(results[0] == 0) {
  //           $("#notifications").removeClass().addClass('alert alert-danger').html(results[1]).fadeIn().delay(5000).fadeOut();
  //           $("html, body").animate({ scrollTop: 0 }, "slow");
  //           var line_id = '#'+lid;
  //           $(line_id).find('input[name="quantity"]').val(parseInt(input_value) - 1);
  //       } else {
  //           window.location.replace(URL_BASE + '/cart');
  //       }
  //   });
  // });

  $(window).scroll(function () {
    var scrollTop = $(window).scrollTop();
    // console.log(scrollTop);

    if (scrollTop < 100 && ACTION == 'detalhe') {
      $('.header-body').addClass('transparent-menu');
    } else {
      $('.header-body').removeClass('transparent-menu');
    }
  });

  $(".custom-file-input").on("change", function() {
    var fileName = $(this).val().split("\\").pop();
    $(this).siblings(".custom-file-label-frutasclasse").addClass("selected").html(fileName);
  });

});
